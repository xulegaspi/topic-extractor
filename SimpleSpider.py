__author__ = 'Xurxo'
import urllib
from bs4 import BeautifulSoup
import urlparse
import mechanize


# Set the starting point for the spider and initialize
# the a mechanize browser object
url = "http://www.engadget.com"
max_links = 20
rules_yes = "engadget" and "2015"
rules_no = "mailto" and "#" and "?"
filename = "dataset_engadget.txt"

br = mechanize.Browser()

# create lists for the urls in que and visited urls
urls = [url]
visited = [url]
depth = [0]

file = open(filename, 'w')
# Since the amount of urls in the list is dynamic
#   we just let the spider go until some last url didn't
#   have new ones on the web page
while 0 < len(urls) < max_links:
    try:
        htmltext = urllib.urlopen(urls[0]).read()
    except:
        print urls[0] + " -> ERROR"
    soup = BeautifulSoup(htmltext, "html.parser")

    urls.pop(0)
    deep = depth[0]
    print deep
    depth.pop(0)
    # print len(urls)

    for tag in soup.findAll('a', href=True):
        tag['href'] = urlparse.urljoin(url, tag['href'])
        if url in tag['href'] and tag['href'] not in visited:
            if rules_yes in tag['href'] and rules_no not in tag['href']:
                urls.append(tag['href'])
                depth.append(deep + 1)
                visited.append(tag['href'])
                file.write(tag['href'] + "\n")
                # print tag['href']

# print visited

