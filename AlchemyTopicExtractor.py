__author__ = 'Xurxo'
from alchemyapi.alchemyapi import AlchemyAPI
import json
from datetime import datetime

max_links = 500
in_filename = "output_engadget.txt"
details_out_filename = "results/details/data_article_"
out1_filename = "results/data_mined_rows.json"
out_filename = "results/data_mined.json"
ext = ".txt"

alchemyapi = AlchemyAPI()
counter = 1
final_data = []

out1 = open(out1_filename, 'w')

with open(in_filename, 'r') as fp_urls:
    for url in fp_urls:
        response = alchemyapi.title('url', url)
        if response['status'] == 'OK':
            title = response['title']

        response = alchemyapi.pubdate('url', url)
        if response['status'] == 'OK':
            pubdate = response['publicationDate']
            oldtime = pubdate['date']
            try:
                date_object = datetime.strptime(oldtime, '%Y%m%dT%H%M%S')
                form_date = date_object.strftime('%d-%m-%Y')
                # print form_date
            except:
                form_date = ""

        response = alchemyapi.text('url', url)
        if response['status'] == 'OK':
            text = response['text']

        categories = []
        scores = []
        comb = []
        response = alchemyapi.taxonomy('url', url)
        if response['status'] == 'OK':
            taxonomy = response['taxonomy']

            my_var = '{"id": "' + str(
                counter) + '", "url": "' + url[:-1] + '", "title": "' + title + '", "pubDate": ' + json.dumps(
                pubdate) + ', "categories": ' + json.dumps(taxonomy) + '}'
            print my_var
            final_data.append(json.loads(my_var))

            # Writing in row-based file for easy reading
            printable = json.loads(my_var)
            out1.write(json.dumps(printable) + "\n")

            # Writing in details files
            with open(details_out_filename + str(counter) + ext, 'w') as out2:
                out2.write(str(counter) + "\n")
                out2.write(url)
                out2.write(title + "\n")
                out2.write(form_date + "\n")
                out2.write(text.encode('utf-8') + "\n")
                for category in taxonomy:
                    out2.write(json.dumps(category) + "\n")
                out2.close()

        counter += 1
        if counter == max_links:
            break

    # Writing in JSON file
    with open(out_filename, 'w') as out:
        out.write(json.dumps(final_data))