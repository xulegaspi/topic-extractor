14
http://www.engadget.com/2015/05/16/spacex-mars-vintage-posters/
SpaceX's Mars travel posters make us want to explore the red planet
16-05-2015
SpaceX has released a few Mars travel posters in the same vein as NASA's vintage exoplanet ones, and they're making us sad we were born too early for space exploration. Valles Marineris the "land of Martian chasms and craters," as the poster says, is an enormous series of canyons. Olympus Mons is tallest peak and biggest volcano we've seen in the solar system, thus far, at thrice the height of Mt. Everest. Finally, Phobos and Deimos are the planet's two moons, though we've explored the former more between the two. Elon Musk announced earlier this year that he doesn't only plan to go to Mars, he also wants to establish a city on the red planet. We might all be too old -- or you know, too dead -- to live there or visit when the time comes, but maybe these posters can help you imagine how it would be for our children's children (sniffs).
[Image credit: SpaceX/Flickr]

{"score": "0.577402", "label": "/family and parenting/children"}
{"score": "0.559198", "label": "/travel/tourist destinations/national parks"}
{"score": "0.503392", "label": "/finance/personal finance/lending/credit cards"}
