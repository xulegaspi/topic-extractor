16
http://www.engadget.com/2015/07/02/elon-musk-future-of-life-safe-ai/
Elon Musk-backed project grants $7 million to AI researchers
02-07-2015
After Elon Musk donated $10 million to the Future of Life Institute (FLI) to finance studies aiming to keep AIs safe and beneficial (i.e., prevent them from going down Skynet's path) almost 300 teams submitted their research proposals. Now, the institute is finally done reviewing them all and has decided to grant $7 million from Musk and the Open Philanthropy Project to 37 projects over the next three years. Some of the studies want to teach AI what humans prefer based on body language, one aims to develop a system that can explain its decision to humans, while another vows to figure out how to make sure robots and other intelligent weapons are always kept under human control.
The recipient getting the biggest amount ($1.5 million) proposes a joint Oxford-Cambridge research center to study the long-term impacts of AI, among other things. It will also develop policies to "minimize risks and maximize benefit[s] from artificial intelligence development." The organizers want to clarify, though, that they're not fearmongering, especially now that Terminator Genisys is about to be shown in theaters. "The danger with the Terminator scenario isn't that it will happen, but that it distracts from the real issues posed by future AI," FLI president Max Tegmark said. "We're staying focused, and the 37 teams supported by today's grants should help solve such real issues." The institute listed all awardees right here, along with a summary of their proposals, many of which would have sounded like science fiction just a few years ago.

{"score": "0.682174", "label": "/finance/grants, scholarships and financial aid/government grants"}
{"score": "0.456236", "label": "/law, govt and politics/government"}
{"confident": "no", "score": "0.262209", "label": "/business and industrial/business operations/management/project management"}
