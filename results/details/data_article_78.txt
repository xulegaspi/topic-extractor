78
http://www.engadget.com/2015/12/18/spacex-falcon9-launch/
SpaceX is on target for a launch-and-land attempt Sunday
18-12-2015
We're eagerly awaiting SpaceX's next launch of a Falcon 9 rocket, and CEO Elon Musk tweeted tonight that it "will aim to launch Sunday." This is also the company's first attempt since a rocket exploded shortly after takeoff in June, and another failed to land on an ocean barge in April. A static fire test that took place earlier today "looks good," so if all the data checks out then this could be SpaceX's time to take the reusable rocket crown back from Blue Origin with a ground landing (or at least launch eleven OG2 satellites).
Update: In a second tweet this morning, Musk provided more detail, saying the company plans an "attempted orbital launch and rocket landing at Cape Canaveral" Sunday night around 8PM ET.

{"score": "0.778662", "label": "/business and industrial/aerospace and defense/space technology"}
{"confident": "no", "score": "0.432158", "label": "/business and industrial/advertising and marketing/marketing"}
{"confident": "no", "score": "0.365538", "label": "/law, govt and politics/government"}
