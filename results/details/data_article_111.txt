111
http://www.engadget.com/2015/09/20/pluto-aerial-tour/
How to join NASA's festivities as New Horizons flies by Pluto
20-09-2015
Alas, you're unlikely to soar over Pluto's alien landscape any time soon -- not when it's billions of miles away. However, NASA has whipped up an aerial tour that will give you an inkling of what it might be like. The simulated flyover (below) uses the New Horizons probe's latest high-res imagery to show off Pluto with an "unprecedented" level of detail of up to 0.25 miles per pixel). It's certainly not the same as being there, but it's not hard to imagine sitting in a spacecraft orbiting the dwarf planet. And don't worry if this doesn't meet your expectations. New Horizons will eventually send pictures that are up to seven times sharper, so you can expect more impressive virtual flights in the months ahead.

{"score": "0.677378", "label": "/technology and computing/consumer electronics/camera and photo equipment/telescopes"}
{"confident": "no", "score": "0.357586", "label": "/society/dating"}
{"confident": "no", "score": "0.309493", "label": "/business and industrial/aerospace and defense/space technology"}
