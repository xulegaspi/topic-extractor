26
http://www.engadget.com/2015/11/28/nsa-bulk-nsa-phone-surveillance-ends/
The NSA's mass US phone surveillance ends tonight
28-11-2015
The National Security Agency's long-running mass phone surveillance program is coming to an end. As promised, the USA Freedom Act will forbid the NSA from indiscriminately collecting Americans' call metadata at midnight on November 29th. Agents will have to get court orders to collect data from telecoms regarding specific people or groups, and then only for six months at a time -- they can't just scoop up everything in case something useful turns up. The NSA will still have access to five years' worth of legacy data through February 29th, but that's as far as its access will go.
The new approach doesn't affect foreign intelligence gathering or internet data collection programs like PRISM, and it won't do much to hold the NSA more accountable for its actions. Nonetheless, the shut down remains a big deal. It's the first time in a long while that the NSA is losing surveillance powers, after all. This moment is also a vindication of sorts for Edward Snowden, whose whistleblowing revealed the NSA's true reach and prompted the Act's existence. Whatever you think of Snowden's actions, there's no question that he's ultimately responsible for these policy changes.

{"score": "0.998075", "label": "/law, govt and politics/espionage and intelligence/surveillance"}
{"confident": "no", "score": "0.37753", "label": "/technology and computing/hardware/computer"}
{"confident": "no", "score": "0.335487", "label": "/hobbies and interests/collecting"}
