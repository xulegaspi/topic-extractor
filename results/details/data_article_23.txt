23
http://www.engadget.com/2015/12/05/un-draft-climate-change-deal/
Draft climate change deal lowers greenhouse gases by 2050
05-12-2015
Earth just took a tentative step toward a new, comprehensive plan for improving the environment.  Representatives from 195 countries have approved a draft UN climate change agreement that will ask all participants to lower their carbon dioxide emissions.  There are still many, many details left to resolve ahead of a final deal (ideally signed next week), but the ultimate goal is to have countries reduce their greenhouse gas levels by 2050, and to eliminate emissions completely between 2060 and 2080.
They'll approach the problem from multiple angles.  Countries are not only pledging to cooperate on eco-friendly technology, but to contribute tech, financing and other assistance to developing nations that need a helping hand.  There should also be incentives against deforestation, as well as progress checks every 5 years starting in 2024.
Whether or not the agreement works is another matter.  Remember the Kyoto Protocol, where numerous countries (such as China and the US) either didn't have binding emissions targets or didn't ratify the deal?  It wasn't nearly as effective as some were hoping for.  Also, the exact emissions target and other key points are still up in the air.  The pact could prove effective if it holds governments to meaningful goals, but it may be relatively toothless if the targets are either relatively soft or include loopholes to suit certain countries' economic agendas.

{"score": "0.608406", "label": "/health and fitness/disease/cholesterol"}
{"score": "0.562629", "label": "/science/mathematics/geometry"}
{"score": "0.431778", "label": "/art and entertainment/movies and tv/movies"}
