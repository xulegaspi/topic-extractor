83
http://www.engadget.com/2015/07/28/ntsb-spaceshiptwo-crash-findings/
Virgin Galactic's SpaceShipTwo crash was due to co-pilot error (updated)
28-07-2015
After nearly 9 months of investigation, the National Transportation Safety Board has an official explanation for Virgin Galactic's SpaceShipTwo crash. As suspected, the accident happened when the co-pilot triggered the "feathering" system (moving the tail wings to increase drag for reentry) well below the intended Mach 1.4 speed -- the premature resistance led to the suborbital craft breaking up and plummeting into the Mojave Desert. More details are forthcoming, but Virgin Galactic says that it welcomes the findings. Hopefully, the lessons learned prevent future accidents and keep private spaceflight on track.
Update: The NTSB has published the full ruling, and says that there also wasn't enough done to either prevent this mistake or educate pilots about what would happen.  Even the FAA is partly to blame, since it didn't check to make sure that the requirements behind a hazard waiver were implemented properly.  In other words, the co-pilot's slip-up was the last piece of a larger puzzle.

{"confident": "no", "score": "0.365714", "label": "/law, govt and politics/politics/elections"}
{"confident": "no", "score": "0.317763", "label": "/hobbies and interests/games/board games and puzzles"}
{"confident": "no", "score": "0.278519", "label": "/travel/transports/air travel/helicopters"}
