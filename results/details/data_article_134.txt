134
http://www.engadget.com/2015/10/12/fanduel-draftkings-fantasy-sports-scandal/
DraftKings and FanDuel sue to stop New York Attorney General's ban
13-11-2015
Those suspicions that DraftKings and FanDuel employees are profiting from being a little too cozy?  Yeah, they've just gained some serious credibility.  On top of a recent lawsuit accusing the two websites of using insider information to commit fraud, the New York Times has learned that staff at these sites have frequently been raking in huge amounts of cash by playing on each other's sites.  Both DraftKings and FanDuel workers have "regularly" ranked as big winners, possibly by using that insider info to gain an unfair advantage.  DraftKings co-founder Paul Liberman even noted that some of his team members have made "significantly more money" from competing at FanDuel than they have working their jobs.  Taking that option away might discourage people from staying with the company, he claimed.
The companies are trying to mend their image.  They've both banned their employees from playing in daily fantasy sports of any kind, and the firms are conducting an independent investigation that should report back within weeks.  Even if DraftKings and FanDuel clean up their playing practices, though, neither the inspection nor the lawsuit are likely to represent the end of the story.  The sites are operating in a largely unregulated field, and it won't be surprising if the US government eventually clamps down to make sure that everyday players stand a real chance of winning.

{"score": "0.875531", "label": "/sports"}
{"confident": "no", "score": "0.531433", "label": "/technology and computing/internet technology/web search"}
{"confident": "no", "score": "0.491357", "label": "/business and industrial/advertising and marketing/public relations"}
