120
http://www.engadget.com/2015/10/31/vodafone-bank-details-attack/
The US and China want to set ground rules for cyberwarfare
23-10-2015
In what's been a big week for corporate security following the TalkTalk hack, Vodafone has confirmed that it has also been the target of a malicious attack. In a statement today, the company said that between midnight on October 28th and midday on the 29th, it saw an unauthorised party attempt to access customer's details, including bank account numbers. Unlike its telecoms rival, which saw its website compromised via SQL injection, the carrier believes the source of the attack stems from criminals utilising "email addresses and passwords acquired from an unknown source external to Vodafone."
In total, 1,827 customers had their accounts accessed, which could have given attackers access to names, mobile numbers, bank sort codes and the last four digits of their bank account. "No credit or debit card numbers or details were obtained," the company notes. "However, this information does leave these 1,827 customers open to fraud and might also leave them open to phishing attempts."
With the targeted accounts already blocked, Vodafone says it has begun contacting customers who may have been affected. Banks have been put on alert for fraud and the National Crime Agency (NCA), the Information Commissioner's Office (ICO) are working to identify the cause of a potential early leak and ensure that all necessary procedures are followed.
Just yesterday, TalkTalk confirmed that 1.2 million customers had their details stolen. With no intrusion detected, 1,827 people may be alarmed when they learn about what happened to their Vodafone accounts, but it's a very small attack in comparison. Customers will need to be on their guard for phishing attempts, where attackers attempt to lure more information from a customer using details they have acquired, but the malicious party won't be able to clear their bank accounts or take money from them.

{"score": "0.519302", "label": "/technology and computing/consumer electronics/telephones/mobile phones"}
{"score": "0.509305", "label": "/finance/personal finance/lending/credit cards"}
{"score": "0.500586", "label": "/technology and computing/internet technology/isps"}
