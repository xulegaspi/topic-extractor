http://www.xataka.com/moviles/galaxy-s6-y-s6-edge-todas-las-claves-de-los-nuevos-gama-alta-de-samsung-con-video
Gadgets y tecnología: últimas tecnologías en electrónica de consumo - XATAKA






Buscar »

Google+
@solenoide
Lead editor en Xataka
Así es cómo se consigue convertir un tablet en un equipo de sonido 5.1
Los tablets tienen un problema serio con el sonido. ¿Por qué suena tan enlatado? Así es la solución de Huawei.


Pocas dudas hay ya de que el Samsung Galaxy S6 y su hermano el Galaxy S6 Edge con su pantalla de tres lados aspira a convertirse en el terminal más destacado del MWC 2015. 
Había que centrarse pues en él y allá que nos hemos ido a cogerlo con banda para contarte, vídeo incluido, las claves y datos que tienes que saber sobre los nuevos smartphones de gama alta de Samsung.
Lo primero que ha atraído la atención del Galaxy S6 y S6 Edge en el MWC 2015 ha sido su diseño. En el caso del modelo Galaxy S6, no tenemos muchas dudas de que se ha inspirado en algún que otro aspecto en el iPhone 6, especialmente el borde inferior, pero también recoge mucha de la experiencia que ha ido probando estos últimos meses. Pese a ello, hay que reconocerle a Samsung que la elección de cristal y marco metálico les ha quedado muy bien. 
Esas novedades en el diseño trae consigo algunos compromisos que ha hemos visto que no dejan a nadie indiferente. También en la parte trasera la cámara, que tiene mucho protagonismo en este terminal, sobresale bastante. 

Algo que se aprecia nada más sostener un rato el nuevo Galaxy S6 es que la elección del cristal hace que el terminal deslice mucho más en mano y la suciedad queda mucho más visible, dependiendo también del color que escojamos. En el blanco por ejemplo pasa más desapercibido. Pese a ello, es un equipo que por grosor y peso da muy bien en mano.

Todavía mejor nos ha parecido mucho mejor diseñado y acabado el Galaxy S6 Edge. Como pasó con el Note, Samsung sí que está liderando innovaciones en la pantalla curvada, y con el diseño el Galaxy S6 Edge creemos que ha conseguido un punto de partida y diferenciación muy notable que le da al terminal bastante personalidad.

En este terminal, más que los materiales usados, lo que más aporta al diseño es la doble curva de la pantalla en los laterales. Pero ya no nos ha parecido tan ambiciosa en querer hacer de todo sin pies ni cabeza. Ahora, lo que se permite hacer deslizando el dedo desde los laterales tiene más sentido, y está muy dirigido a gestionar llamadas, notificaciones y contactos. Pese a ello, el toque de distinción que le da la curva al diseño nos parece ya de por sí un acierto.

La delgadez que lo hace muy cómodo en mano ha provocado que la batería tenga que ser de una capacidad contenida. Por eso resulta básico que cuente con un modo de carga rápida, que en 80 minutos pasa de 0 a 100% de la carga en los dispositivos, pero también nos ha gustado que, en el diseño atractivo vaya integrada de serie la carga inalámbrica. En este caso son tres horas las que empezaríamos para una carga completa de los dispositivos. 
Otra mejora que nos trae el Galaxy S6 y S6 Edge es el lector de huellas. Ahora no hay problema en la orientación y realmente creemos que el valor de esta funcionalidad en los terminales de Samsung empieza ahora, porque nos ha parecido muy sencilla y efectiva la identificación. Cómo no, la pantalla, con una resolución 2K, se ve asombrosa incluso con luz intensa, y el panel AMOLED lo da todo en contraste y colorido.

El rendimiento también promete mucho gracias a la nueva RAM, la mejor memoria interna y los procesadores propios. Ayuda sin duda que, pese a contar con toques TouchWiz, se haya implicado la interfaz. La gran duda estará con la batería, pero enseguida lo comprobaremos.
Lo que Samsung ha dicho sobre la rapidez de su cámara no es un comentario cualquiera. Lo hemos podido comprobar y es una delicia y necesidad poder tener la cámara lista para disparar desde cualquier lugar en que nos encontremos. Podemos por ejemplo con doble toque sobre el botón de inicio acceder a la cámara con el terminal inactivo. 
 


            Ver galería completa » Galaxy S6 imágenes (8 fotos)
    














































 window.addEventListener('load', function(){
    $("#carousel-0").elastislide();
 });


La interfaz ha sido potenciada con más controles, aunque todavía no es un modo manual completo. A nivel de calidad, lo que hemos podido probar nos ha convencido, tanto en la cámara principal como en la secundaria. La cámara del Galaxy S6 es muy rápida también enfocando y disparando, así que, si unimos esa facilidad para el apuntar y disparar, con mejor calidad de imagen, hay cámara a tener en cuenta en este 2015.
Las características técnicas de la cámara nos dejan con el mismo sensor que en el Note 4, con sus 16 megapíxeles y estabilización óptica, pero mejorando la apertura a f1.9 y el enfoque con un modo AF de seguimiento. El balance de blancos también ha sufrido modificaciones con un detector IR para conseguir más precisión en el color. 

A partir del 10 de abril ya se podrá comprar el nuevo Samsung Galaxy S6 y S6 Edge, con precios que son de 699, 799 y 899 euros para las versiones de 32, 64 y 128 GB para el Galaxy S6, y un poco más, 849, 949 y 1049 euros para el modelo con curvas en su pantalla.
Si te has quedado con ganas de más, muy pronto podrás leer la review completa del Galaxy S6 y S6 Edge en Xataka.
También te puede gustar
Recomendado en Magnet
+ en Xataka
Tecnología
Estilo de vida
Motor
Ocio
Economía
Latinoamérica
Participamos en
