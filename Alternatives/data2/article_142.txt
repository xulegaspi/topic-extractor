http://www.xataka.com/moviles/ios-9-nos-descubre-la-probable-inclusion-de-un-pantalla-con-force-touch-en-el-iphone-6s
Gadgets y tecnología: últimas tecnologías en electrónica de consumo - XATAKA






Buscar »

Google+
@javipas
Editor senior en Xataka
Así es cómo se consigue convertir un tablet en un equipo de sonido 5.1
Los tablets tienen un problema serio con el sonido. ¿Por qué suena tan enlatado? Así es la solución de Huawei.


En apenas un mes descubriremos en el WWDC 2015 las novedades que Apple prepara en el segmento del software y los desarrolladores, pero algunas de esas sorpresas han ido desgranándose en las últimas semanas. Fuentes cercanas a la empresa revelan ahora que la próxima versión de la plataforma móvil de Apple, iOS 9, ha permitido dar aún más solidez a uno de los rumores que más se han repetido sobre los futuros iPhone 6S. 
Sea ese o no el nombre definitivo de los futuros smartphones de Apple, todo apunta a que estos dispositivos integrarán la tecnología Force Touch en sus pantallas, lo que hará que la respuesta háptica sea una de sus novedades destacadas. Parece que iOS 9 será especialmente ventajoso en este apartado para los desarrolladores, que podrán aplicar este tipo de nueva interacción táctil a sus aplicaciones móviles. 
Se habla de que Force Touch permitirá liberar parte del espacio dedicado a los controles en el dispositivo y reemplazar algunas interacciones clásicas basadas en la presión larga de botones o iconos. Y por supuesto permitirá acceder a las ventajas que ya vimos en los nuevos MacBook y en los MacBook Pro que ya cuentan con esta tecnología, y que permiten por ejemplo controlar la reproducción de contenidos de forma interesante mediante la aplicación de distintos niveles de presión sobre la pantalla. 

No es probable que Apple hable de Force Touch en el WWDC, y no lo es por la sencilla razón de que eso haría que mostrase sus cartas y desvelase una de las probables sorpresas de los futuros iPhone, algo que es raro que haga. La implementación de esta característica podría afectar no solo a los iPhone, sino también a los iPad, que sin duda darían un salto importante a nivel de interacción con este tipo de novedad. 
Hay más rumores sobre lo que integrará iOS 9, y ya se habla de un teclado mejorado -podrían haber cambiado el comportamiento de la tecla Shift, una de las viejas demandas de los usuarios- del soporte de Apple Pay en Canadá -de momento nada fuera de los EE.UU.- o de su plataforma de mensajería, iMessage. Sin olvidar las mejoras en estabilidad y rendimiento, la esperada inclusión de soporte para aplicaciones a pantalla partida en los iPad o una nueva aplicación Home para controlar accesorios domóticos que sigan el estándar HomeKit. Estaremos muy atentos a todas esas novedades. 
Vía | 9to5Mac
En Applesfera | ¿De verdad necesitamos iOS 9 en 2015?
También te puede gustar
Recomendado en Magnet
+ en Xataka
Tecnología
Estilo de vida
Motor
Ocio
Economía
Latinoamérica
Participamos en
