http://www.xataka.com/moviles/asi-queda-el-ranking-de-fabricantes-de-smartphones-en-2014-segun-idc
Gadgets y tecnología: últimas tecnologías en electrónica de consumo - XATAKA






Buscar »

Google+
@javipas
Editor senior en Xataka
Así es cómo se consigue convertir un tablet en un equipo de sonido 5.1
Los tablets tienen un problema serio con el sonido. ¿Por qué suena tan enlatado? Así es la solución de Huawei.


La consultora IDC ha publicado un informe en el que se resume en cifras cómo fue el mercado de los smartphones durante el pasado año 2014. Entre las cifras destaca la desaceleración de Samsung y el crecimiento espectacular de un participante al que casi nadie esperaba: Xiaomi. 
Según esos datos se distribuyeron 375,2 millones de smartphones durante el cuarto trimestre de 2014 (un 28,2% más que en el mismo trimestre de 2013) para completar un total de 1.301,1 millones de de terminales distribuidos en 2014. El crecimiento en el mercado es sorprendente y se sitúa en un 27,6% respecto a los 1.019,4 millones de 2013. De saturación en el mercado de smartphones, nada.
Los analistas de IDC indican que Apple podría superar a Samsung como principal distribuidor de smartphones en todo el mundo a lo largo de 2015, algo que queda patente tras un último trimestre en el que las ventas de los iPhone de Apple han sido espectaculares y en las que Samsung sólo ha podido mantener el tipo de forma muy justa. 

En el último trimestre de 2014 el ránking de los cinco principales participantes en el segmento de los smartphones ha tenido un protagonista especial en Xiaomi, el fabricante local chino que ha crecido un 178,6% en unidades distribuidas con respecto el año anterior y se ha convertido en toda una potencia a nivel mundial a pesar de que la inmensa mayoría de sus terminales no salen de China. 
Ese crecimiento de momento no le coloca entre los cinco primeros del mundo a lo largo de 2014: en los pasados 12 meses el ránking ha estado encabezado por Samsung con un 24,5% de cuota de mercado: uno de cada cuatro dispositivos comprados en todo el mundo llevaban el logotipo del fabricante surcoreano. Por detrás, no ya tan lejos, está Apple, que con un 14,8% de cuota de mercado curiosamente ha bajado en ese porcentaje (en 2013 llegó a tener el 15,1%) pero no para de crecer en ventas. 
Ello se debe a algo que IDC no muestra en detalle: los porcentajes por debajo del quinto puesto han cambiado sensiblemente, y el apartado "Otros" ha pasado de ocupar el 40,0% de cuota a ocupar el 45,1% en 2014. En esa cifra a buen seguro Xiaomi tiene mucho que decir, y probablemente veamos cambios en el informe que IDC publicará dentro de un año y en el que puede que esa categoría "Otros" sea aún más importante. 

Mientras tanto gigantes como Huawei, Lenovo y LG siguen manteniendo sus posiciones privilegiadas. En el caso de Lenovo IDC tiene la amabilidad de darnos datos con y sin Motorola incluida. En el caso de incluir a Motorola, su cuota crecería del 5,4% al 7,4%, pero prevemos (al menos, yo lo hago) que el papel de Motorola será mucho más importante para Lenovo en los próximos 12 meses que ya hemos empezado a vivir. 
Todos los fabricantes están en positivo en unidades distribuidas en 2014 y todas han crecido en ese apartado, pero otra historia distinta es la de los ingresos y el negocio conseguido, que IDC no refleja y que precisamente permite comprobar quién está mejor preparado para este 2015. 
Samsung está sufriendo muchos varapalos últimamente y veremos cómo reacciona, y aunque parece complicado que Apple supere de momento en unidades distribuidas (sus 192,7 millones de iPhones quedan lejos aún de los 318,2 de Samsung), hay otros frentes abiertos para la empresa surcoreana, que desde luego se enfrenta a un 2015 que podría ser decisivo en su historia. 
En Xataka | IDC: Android e iOS ya están en el 96,4% de los smartphones
También te puede gustar
Recomendado en Magnet
+ en Xataka
Tecnología
Estilo de vida
Motor
Ocio
Economía
Latinoamérica
Participamos en
