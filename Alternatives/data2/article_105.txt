http://www.xataka.com/moviles/xiaomi-ha-vendido-61-millones-de-telefonos-en-2014
Gadgets y tecnología: últimas tecnologías en electrónica de consumo - XATAKA






Buscar »

Google+
@kotecinho
Editor senior en Xataka
Así es cómo se consigue convertir un tablet en un equipo de sonido 5.1
Los tablets tienen un problema serio con el sonido. ¿Por qué suena tan enlatado? Así es la solución de Huawei.


Lo de Xiaomi tiene poca explicación, yo no imaginé que aquel teléfono que llegó a mis manos en febrero de 2012 iba a convertirse en la primera piedra de un camino meteórico. Sin decir nombres, se me ocurren varias empresas más grandes y con más historia, que no son capaces de vender productos al mismo nivel.
Lei Jun, fundador y CEO del fabricante chino ha anunciado hoy cifras de ventas y algunos datos económicos relativos al año que acabamos de cerrar. Lo más llamativo, los 61,1 millones de teléfonos que han hecho llegar a manos de clientes.
Como siempre, la cifra cobra mayor sentido si miramos que en 2013 vendieron 18,7 millones - 7,2 millones en 2012 -. Hablamos de un crecimiento del 326% con respecto al año anterior, que hace a pocos dudar sobre la meta de los 100 millones en 2015.  
Xiaomi 2014: 61.1 million phones sold, ¥74.3B in revenue (nearly $12B)— Hugo Barra (@hbarra) enero 4, 2015
Xiaomi 2014: 61.1 million phones sold, ¥74.3B in revenue (nearly $12B)
Sobre ingresos, pues la compañía ha registrado 11.970 millones de dólares, lo que supone doblar los números de 2013. No se habla sobre beneficios, que posiblemente sea el aspecto más débil en una compañía relativamente joven, que busca más hacerse un hueco en el mercado.
La empresa china que el resto copia. La estrategia de vender online y crear comunidad la vamos a ver repetida bastante en 2015Es una realidad que el resto de competidores le están copiando el negocio, con terminales asequibles que solo se venden a través de la red. Huawei y Lenovo ya tienen en funcionamiento la misma idea, veremos a ver qué tal les sale la aventura. 
La empresa china que el resto copia. La estrategia de vender online y crear comunidad la vamos a ver repetida bastante en 2015
Con respecto a esta situación, Lei Jun considera que deben darle una vuelta a la estrategia, y en la medida de lo posible, reinventarse.
Tan joven, y tan grande en cuota de mercado. Los números son complicados de comparar sin que tengamos a mano cifras acumuladas de Lenovo y Motorola del 2013, pero están en un nivel similar de ventas. Pelean por un tercer escalón del ranking de ventas, solo por detrás de Samsung y Apple.

Actualmente operan en siete mercados asiáticos, con la intención de seguir creciendo por el sudeste del continente, pero también fuera de él. Se habla de México, Turquía y Brasil. En lo que respecta a China, se espera que este año superen a Samsung como principal vendedor.
Un mercado clave para Xiaomi es la India, como lo es para el resto de fabricantes chinos. Hay mucho margen de crecimiento en él, y los primeros pasos han sido prometedores, con más de un millón de unidades vendidas en cinco meses. Unas ventas truncadas por un problema de patentes con Ericsson.
Sobre teléfonos, gran parte de culpa está en su gama más accesible, que ha sido renovada hoy con el Xiaomi Redmi 2S, pero también con un potente buque insignia  conocido como Mi 4, que muy posiblemente reciba un sucesor el próximo 15 de enero.
Xiaomi cuenta actualmente con 5.000 empleados, y como muchos conocéis, hace muchas más cosas que crear teléfonos. Al margen de la expansión, la idea es crear una compañía que toque todos los palos en la electrónica de consumo, un ecosistema Xiaomi mantenido por cien departamentos.
En Xataka | Xiaomi, el camino diferente en la industria china
También te puede gustar
Recomendado en Magnet
+ en Xataka
Tecnología
Estilo de vida
Motor
Ocio
Economía
Latinoamérica
Participamos en
