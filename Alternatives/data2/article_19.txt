http://www.xataka.com/moviles/huawei-mate-8-primeras-impresiones-una-enorme-y-potente-actualizacion
Gadgets y tecnología: últimas tecnologías en electrónica de consumo - XATAKA






Buscar »

Google+
@Lohar
Editor en Xataka
Así es cómo se consigue convertir un tablet en un equipo de sonido 5.1
Los tablets tienen un problema serio con el sonido. ¿Por qué suena tan enlatado? Así es la solución de Huawei.

El día de hoy Huawei presentó en el marco del CES 2016 la renovación de uno de sus equipos insignia, el Mate 8, con el que perfeccionan mucho de lo que hemos visto en la serie Mate, pero también en el Nexus 6P, pero ahora con un diseño atractivo y materiales de gran calidad, eso sí, con el mismo gran tamaño que la generación anterior.
Durante este CES 2016 tuvimos oportunidad de probar por unos minutos este nuevo Mate 8, donde hemos visto un poco de lo que tiene pensado Huawei dentro de la gama alta para este 2016 y a continuación, tienen nuestras primeras impresiones.

Lo primero que salta a la vista de este Mate 8 es su construcción que roza en la perfección, un pieza de aluminio cepillado que permite tener mayor agarre a pesar del gran tamaño, la parte trasera posee una leve curvatura que aporta mayor ergonomía y control.
De igual forma, en la parte trasera nos encontramos con el sensor dactilar que ahora incorpora nuevas funciones, como botón para selfies, entre otras cosas. La parte trasera se mantiene limpia, sólo veremos el sensor de huellas que acompaña a la cámara y el Flash Dual LED, pero nada más, el aspecto es atractivo y minimalista.
En el frontal el panel de 6 pulgadas aprovecha bien el espacio, dejando casi unos marcos imperceptibles en los laterales, mientras que el superior e inferior se han reducido, esto hace que la pantalla gane protagonismo y se casi pueda operar con una mano.
Al día de hoy, un smartphone gama alta debe estar acompañado por una cámara que destaque, y en las primeras impresiones de la cámara de este Mate 8 los resultados han sido impresionantes, la activación es inmediata, incluso desde la pantalla de bloqueo, cuenta con un montón de opciones para "embellecer" nuestras fotos, además de posee nueva tecnología de procesado que hace que las fotos en bajas condiciones de luz tengan un mejor aspecto.
El enfoque es rápido y capta la luz necesaria sin quemar la toma, mientras que los resultados capturando vídeo son buenos gracias a que posee estabilizador óptico.
Sin duda un punto destacable, es que el Mate 8 incorpora el nuevo SoC Kirin 950, confeccionado por la misma Huawei, que en los primeros benchamarks ha mostrado parte de su poder, y en nuestras primeras pruebas así ha sido, se mueve con soltura, el cambio entre apps es ágil, así como la navegación entre menús.
La capa de personalización Emui 6.0 3.0 nos pone al alcance herramientas que añaden valor a la experiencia, pero se extrañan nuevas opciones o una nueva opción de personalización, sólo tenemos nuevos wallpapers y la posibilidad de editar los accesos directos en el centro de notificaciones.
Sin duda el Mate 8 ha sido una muy buena actualización, por supuesto quedamos a la espera de probar a fondo aspectos como el sonido, batería, desempeño en el día a día, entre otras cosas, pero en estas primeras impresiones nos quedamos con muy buen sabor de boca.
Huawei ha sabido perfeccionar sus dispositivos, esto le ha dado la tercera posición como fabricante de smartphones a nivel mundial, puesto ganado en 2015, pero este 2016 empieza muy bien para los chinos, y parece que no bajarán el ritmo.
También te puede gustar
+ en Xataka
Tecnología
Estilo de vida
Motor
Ocio
Economía
Latinoamérica
Participamos en
