http://www.xataka.com/moviles/nexus-6p-analisis#to-comments
Gadgets y tecnología: últimas tecnologías en electrónica de consumo - XATAKA






Buscar »

Google+
@solenoide
Lead editor en Xataka
Así es cómo se consigue convertir un tablet en un equipo de sonido 5.1
Los tablets tienen un problema serio con el sonido. ¿Por qué suena tan enlatado? Así es la solución de Huawei.

Google sorprendió el año pasado a todos apostando de lleno por un Nexus que rompía con todo lo anterior. El Nexus 6 era más caro y claramente un terminal de nicho por sus 6 pulgadas de diagonal. 
El gran Nexus de este año hereda el número seis pero cambian bastantes cosas. El Nexus 6P no debe confundirte porque es sobre el papel mucho mejor terminal que el del año pasado y potencial competidor para cualquier Android que se ponga delante de él. En Xataka ya lo hemos probado a fondo y te dejamos con la review completa del Nexus 6P. 
Desde hace una una generación los Nexus más grandes son representantes orgullosos de la gama alta Android. Este Nexus 6P lo corrobora con una ficha técnica contundente en cada uno de sus apartados. Veámoslo resumido en su hoja de especificaciones:
El Nexus 6P ya se puede comprar a partir de 649 euros, un precio clásico ya en esta gama alta. Está disponible en tres tonos (aluminio, grafito o blanco nieve). En nuestro caso hemos probado la unidad en aluminio y nos ha gustado mucho.   
En la caja, cuadrada y bien pensada, encontramos de serie el terminal, los clásicos manuales e información sobre garantías, así como la herramienta para abrir la bandeja de la nanoSIM, un cargador USB tipo C de 15 W (5 V/3 A) y dos cables USB tipo C. Uno de ellos es de tipo C a tipo C y el otro de tipo C a tipo A estándar. Como te contaremos ahora, cuídalos nada más sacarlos de la caja porque son muy preciados hoy en día. 
Y si optas por proteger el terminal, algo que no deberías hacer porque perdería buena parte de su atractivo físico, hay diferentes opciones de fundas oficiales y de terceros de todo tipo, desde las más básicas hasta las de tipo cartera que tan de moda están entre cierto público, quizás no el potencial de este Nexus 6P. 

Nosotros recomendaríamos la oficial, que nos gusta por diseño, por encajar en el terminal como un guante (incluyendo el correspondiente hueco para el lector de huellas trasero) y por su precio no muy exagerado: 35 euros.
Hay que ver qué bien le ha sentado a Google que haya sido Huawei la compañía encargada de la fabricación de este Nexus 6P. El historial de la marca china a nivel de diseño y acabado nos ha ido dejando alguna que otra perla a lo largo de estos años, especialmente en el formato pablet. Y todo ese aprendizaje lo ha plasmado de forma magistral en el diseño y acabado del Nexus 6P.
En nuestras primeras impresiones comentábamos las dudas sobre el modelo acabado en blanco al transmitirnos la idea en un primer contacto de que no era un acabado metálico. O al menos no lo parecía. La unidad que he probado es la que tiene tono aluminio y os puedo asegurar que el acabado es excelente y se suma a un diseño elegante. 
El aluminio anodizado es de primera y el cuerpo de una sola pieza, la unión de frontal de cristal con unos magníficos marcos metálicos nos transmiten una idea completa y sin dudas de que hay un acabado premium que no había tenido nunca un Nexus. De hecho tras esta semana de uso me ha parecido el mejor acabado de un smartphone de Android que ha pasado por mis manos. 
El tacto del terminal es exquisito y nos transmite al mismo tiempo la sensación de acabado noble, pero sin renunciar a ser robusto y resistente. 
En mano el Nexus 6P es muy cómodo. Todo lo que puede ser un phablet de 5,7 pulgadas.  No es el más ligero del mercado, pues pesa casi 180 gramos, pero no es excesivamente ancho y eso facilita mucho el agarre. El grosor de poco más de 7 mm nos parece muy conseguido y colabora junto con la relación tamaño-peso ideal para que nos sintamos muy cómodos manejándolo, aunque necesitarás las dos manos para hacerlo con soltura en la mayoría de las situaciones. 
Huawei no ha descuidado ningún detalle y por ejemplo los botones físicos, todos en el lateral derecho, tienen muy buen tacto y acabado también metálico. El de encendido y bloqueo, con recorrido perfecto, está bien situado en el centro del lateral pero quizás elevado respecto a la que para nosotros sería la colocación ideal. Debajo de él tenemos los controles de volumen.  A destacar el patrón rayado del botón de inicio que nos lo identifica al tacto de inmediato.
Del diseño llama poderosamente la atención la banda superior donde va alojada la cámara principal y el flash doble. Esa cámara sobresale ligeramente pero ni lo parece a la vista ni mucho menos al tacto. Huawei lo ha integrado en una protuberacia que abarca todo el ancho del terminal y le ha dado un diseño diferente con protección Gorilla Glass 4, la misma que la pantalla, aprovechando además para incluir la cámara, el flash doble, las antenas, la conectividad NFC y el sensor de enfoque por láser. 
La buena noticia es que  esa parte que sobresale ligeramente ha sido integrada en el diseño de manera que colocando boca arriba el terminal, no hay problema alguno en usar la pantalla ya que no hay vaivén del terminal ni balanceo de ningún tipo. Ni lateralmente ni longitudinalmente. 
El resto de la trasera es la parte metálica del terminal, de una sola pieza y unido de forma continua con el marco. Solo el logo de Nexus bien visible y el círculo para identificar el sensor de huellas rompen esa continuidad. El resto de conexiones son el puerto de auriculares de 3,5 mm en la parte superior y el puerto de carga microUSB (de tipo C) en la inferior. 
Por último decir que en el frontal destacan los dos altavoces estéreo que le otorgan un diseño quizás demasiado simétrico. Será más de una vez cuando lo saques del bolsillo o cojas de la mesa sin tener muy clara la orientación correcta para iniciarlo. Esos altavoces penalizan ligeramente la proporción de dimensiones-pantalla, pero creo que compensan por la calidad del sonido que podemos obtener con ellos. 
De todo lo dicho en este apartado sobre el diseño y acabado del Nexus 6P tan solo echamos de menos la resistencia a las salpicaduras. Ya no hace falta que deba ser sumergible, no es un uso que se dará a este tipo de terminales, pero que al menos se disponga  una capa de protección en caso de accidente con agua hubiera redondeado el diseño de este Nexus 6P. 
Hoy en día, apostar por una pantalla AMOLED en un terminal de gama alta es arriesgado. La referencia la establece Samsung con sus SuperAMOLED de los Note y Galaxy S6 y es complicado quedar a la altura. Lo es salvo que sea la propia Samsung la encargada de proporcionarte sus paneles de última generación para su smartphone.
Es lo que le ha ocurrido a Google y Huawei, que colocan un panel de 5,7 pulgadas como el del Note 4 de última generación de Samsung, y les sale bien la jugada. La pantalla del Nexus 6P ofrece todo lo que hemos visto hasta ahora con paneles AMOLED de alto nivel: brillo poderoso, colores llamativos pero con calibración correcta y una nitidez que sobrepasa incluso lo lógico en estas diagonales. El panel del Nexus 6P tiene resolución 1440p y nos ofrece más de 500 ppp de densidad.
Un apunte: por defecto, este Nexus 6P no permite modificar el modo de pantalla, pero si la saturación de las pantallas AMOLED no van contigo, hay un modo más contenido (sRBG) que podemos activar si "abrimos" las opciones de desarrollo. Para hacerlo hay que ir a Ajustes --> Información del teléfono y en número de compilación, pulsar siete veces seguidas. Listo. 
En términos generales la pantalla del Nexus 6P es una gozada para la vista cuando nos acercamos. También lo es en exteriores cuando regulamos el brillo al máximo (en manual obtenemos un plus que se agradece), aunque un paso por detrás de paneles IPS con brillo rompedor, pero agradecido por el cristal delgado Gorilla Glass 4 que le ha colocado Huawei y que reduce considerablemente los reflejos de la pantalla tanto en interiores como en exteriores. En cuando al sensor de luminosidad, responde muy rápido y los ángulos de visión son aceptables. 
Una peculiaridad que Google y Huawei han incluido en este Nexus 6P era Ambient Display, algo que nos encantó al probar los últimos Motorola.  Esta tecnología activa la pantalla cuando recibimos una notificación así como cuando cogemos el terminal bloqueado. Aquí se nota bastante que no está tan afinado el funcionamiento como en los terminales de Motorola y esto hace que pierda parte de su atractivo.  
Cuando el Nexus 6P está sobre la mesa, las notificaciones sí que provocan siempre la activación de la pantalla para echar un vistazo a lo nuevo que ha llegado, pero el sistema no funciona de forma tan fiable cuando cogemos el Nexus 6P con la mano. En los Motorola el encendido de la pantalla era instantáneo y con un altísimo grado de acierto. En el modelo de Huawei y Google es algo inconsistente y debe ser un movimiento bastante enérgico el que realicemos al coger el terminal de la mesa para que ese adelanto de lo que nos acaba de llegar o se ha ido acumulando en las notificaciones de la pantalla de inicio se muestren.
Pocas concesiones se ha permitido Huawei para el interior del nuevo Nexus 6P de Google. Sin posibilidad de escoger los Exynos de Samsung ni los A9 de Apple, líderes en rendimiento de este año, le quedaba la opción arriesgada: los Snapdragon 810 de Qualcomm. 
Los ocho núcleos a 2 GHz y los 3 GB de memoria RAM de tipo LDDR4 son combinación que está arriba en rendimiento de forma absoluta gracias a la colaboración de un Android limpio y ligero, pero también plantea escenarios en que el calor es más evidente que con otras soluciones. No es nada alarmante pero está presente. 
El día a día con el Nexus 6P nos ha dejado una de las mejores sensaciones de fluidez de todo el año en terminal de gama alta bajo Android. No hay "lag" alguno y las transiciones, multitarea o cualquier acción que realicemos es instantánea. En las pruebas con programas de benchmarks estos han sido los resultados que hemos obtenido:
Otro elemento del Nexus 6P que es todo velocidad es el lector de huellas. Huawei lo ha colocado en la parte trasera, en una zona del tamaño de la yema de un dedo y justo donde lo apoyamos de forma natural al coger con una mano terminales de estas dimensiones. La configuración en Android 6.0 es muy rápida, y no hay que realizar muchas pasadas para tener una huella registrada en el sistema. 
Luego, en funcionamiento, la identificación es intantánea y muy fiable. Apenas hemos tenido problemas cuando la yema del dedo estaba ligeramente húmeda y de hecho, el sistema trata de identificarte a tal velocidad que algunas veces, al Coger el Nexus 6P y rozar la zona con un dedo, Android lo toma como que has tratado de identificarte y te marca error. 
Todas las ventajas que asociamos a tener la identificación por huella en la parte trasera se pierden si queremos hacer uso de la funcionalidad con el Nexus 6P sobre la mesa. Aquí no hay balanceo porque la trasera es prácticamente plana, y si quieres desbloquear el terminal sin cogerlo de la mesa para realizar cualquier tipo de acción o consulta, no podrás hacerlo y tendrás que introducir la contraseña o patrón que hayas colocado como segunda opción de seguridad. No es nada problemático pero no te olvides de tenerlo en cuenta.
Del resto de especificaciones, el Nexus 6P va bien dotado. La conectividad WiFi es ac 2x2 (MIMO), y nos ha funcionado muy bien a nivel de alcance y velocidad con un router compatible y en zonas de casa donde otros terminales de este año no disponían de buena conexión. También la calidad de las llamadas es fantástica, con sonido claro y alto. Por último no te olvides de que la conectividad NFC está situada en la parte de la cámara, la cubierta con el cristal Gorilla Glass 4. 
La memoria base del terminal es de 32 GB, y hay posibilidad de escoger modelos de 64 y 128 GB. Es lo menos que nos puede plantear un fabricante de un terminal de gama alta a estas alturas. Eso sí, no ha posibilidad de ampliación con tarjetas microSD. Al precio que se están poniendo modelos de memorias microSD muy rápidas y de gran capacidad, es un déficit a añadir a un smarpthone de este tipo. 
A los pocos días de encender el Nexus 6P para la prueba nos llegó la actualización 6.0.1 de Android. Es el ejemplo más claro de lo que significa hoy en día tener un Nexus. Nada de precio contenido y rompedor para con el mercado. Lo que obtenemos en una experiencia Android pura y que, dependiendo del terminal que tengas o la marca que compres, puede que no llegues a ver nunca.
 


            Ver galería completa » Android 6.0 en Nexus 6P (6 fotos)
    




































 window.addEventListener('load', function(){
    $("#carousel-0").elastislide();
 });


Dicho esto, las mejoras de Android 6.0 que ya te contamos, al menos a nivel de usuario, no son tan relevantes como para ese cambio de numeración.  Ello no quita que haya avances importantes  como la mejor, más potente e intuitiva gestión de los permisos para aplicaciones. 
También la mejor gestión de la energía o nuevas funcionalidades como Google Now On Tap le sientan bien a este Nexus.  En éste último caso es una utilidad que uno va apreciando cada día más con el uso y que representa perfectamente el espíritu Google: adelantarse y ofrecerte información que es más fiable cuando más usas sus servicios. 
Este año 2015 va a ser recordado como el último en que los smartphones que no entran en la categoría de phablets han presentado una autonomía más allá de la básica. La familia Sony Xperia ha sido casi la última de esa estirpe, así que toca dar paso a los phablets no solo como referencias en cuanto a tamaño de pantalla sino también como los smartphones con los que poder ir más allá de una jornada de trabajo con soltura en la gama alta. 
El Nexus 6P es un representante perfecto de ese modelo de smartphone que juega la baza de una batería de gran capacidad aprovechando su tamaño y peso, para irse hasta el día y media o incluso dos en autonomía. Aquí los 3450 mAh no se desaprovechan y en combinación con Doze (Android 6.0), proporciona un extra de poderío al referirnos a la autonomía especialmente cuando tenemos el terminal bloqueado. 
Android 6.0 no recurre a modos de ahorro de energía sino a toda una idea para gestionar el consumo cuando no estamos usando de forma directa el smartphone. Y en la prueba es lo que nos ha pasado. En la semana en que hemos probado el Nexus 6P en Xataka,  con mucha carga de notificaciones, cámara, vídeos y mensajería, tanto instantánea como de correo (y llamadas), el terminal de Huawei y Google ha llegado a la jornada y media de uso sin problema alguno. En ese escenario estaríamos hablando de una media de entre 3,5 y 4 horas de pantalla. 
Los días en que el uso de la pantalla se intensificaba, más allá de las 5 horas,  la autonomía del Nexus 6P superaba el día con un remanente de un 20-30%, cifra acorde con otras reviews de referencia. En el lado opuesto, los fines de semana donde el terminal permanece menos tiempo con la pantalla activa y hay menos notificaciones que gestionar (pero más llamadas y uso de la cámara), no es complicado llegar a los dos días.
Antes decíamos que el Nexus 6P no dudaba en arriesgar con una buena capacidad de batería. Esto tiene como consecuencias un peso mayor del terminal, algo que por otro lado un phablets se puede permitir, pero también un tiempo de carga potencialmente alto. Por eso han llegado las cargas rápidas al mercado.
En el caso del Nexus 6P hemos quedado muy satisfechos. Con el cargador que viene incluido de serie (15 W, 5V, 3A), y usando el cable USB tipo C, la batería quedaba al 100% en menos de una hora y media. Y la carga rápida también implicaba  poder disponer de sobre un 25-30% de carga en 15-20 minutos de carga desde cero. 
La contrapartida a esta buena noticia es que si recurríamos a un cargador estándar ese tiempo de carga subía considerablemente, en algunos casos hasta las dos horas y media. De ahí la importancia de cuidar el cargador y el inconveniente de que las ventajas de la carga microUSB de tipo C con esta cargador por ahora se limita casi en exclusiva a lo que viene en el paquete del Nexus 6P de serie.
El conector microUSB tipo C es un gran paso en comodidad de uso por no tener que pensar cómo conectarlo, pero por ahora hasta ahí llega. Es USB 2.0 y no ganamos más que la conexión reversible por ahora. 
El caso es que si de cables microUSB clásicos tenemos repletos los cajones y diferentes lugares donde pasamos algo de tiempo (la oficina, una mochila, el bolso, la guantera del coche), no pasa lo mismo con los tipo C. En nuestra semana de uso no ha habido día en que queríamos cargar el terminal por ejemplo en la cocina, y no ha sido hasta que hemos intentado conectar un cable microUSB  clásico que nos hemos dado cuenta de que no podíamos. Tocaba pues buscarlo en la zona de trabajo y trasladarlo.
Lógicamente esta situación es temporal y en cuanto el USB tipo C sea un habitual en los teléfonos y diferentes gadgets, que pasará más pronto que tarde, el tema del cable más buscado de la casa será historia. Por ahora conviene hacerse con un par de ellos (la propia Google los vende) y si puedes permitírtelo, también un segundo cargador de pared. 
Qué complicado lo tiene a estas alturas un smarpthone de gama alta para destacar en fotografía. Quedarse en la media está al alcance de muchos pero aspirar a estar entre los tres mejores del segmento ya no es tan elemental.  
En esta semana probando a fondo la cámara del Nexus 6P hemos vivido momentos de querer auparlo a lo más alto con otros en que teníamos más dudas. Finalmente lo dejaremos en un buen lugar, seguramente con posibilidades de meterse en el top3 del año, dependiendo de lo que busques en concreto en la cámara de un smarpthone de gama alta.
El potencial del Nexus 6P cuando hay luz suficiente es muy grande. Las fotos que podemos conseguir son fantásticas en muchas ocasiones. El enfoque por láser es muy veloz y efectivo y el disparo instantáneo. Los 12 megapíxeles se quedan en esa cantidad de píxeles bién equilibrados, aunque por si acaso Huawei le ha colocado un sensor de generoso tamaño, mayor que el de la mayoría de gama alta del año. 
En el buen comportamiento del Nexus 6P en exteriores hay algunos matices y casi todos ellos tienen que ver con la excesiva luz. El luminoso sensor del Nexus sufre en situaciones de alto rango dinámico, y hay que afinar bastante en las zonas donde forzamos al terminal a realizar la medición. Aquí, con las limitaciones de la aplicación nativa, es muy recomendable optar por una aplicación de terceros que nos deje compensar la exposición al menos. 
La otra tendencia no tan positiva de la cámara del Nexus 6P es que por norma general tiene a sobreexponer. El ejemplo de abajo es muy significativo: colorido y exposición muy acertadas, detalle y enfoque muy conseguido para ser un elemento en movimiento, pero con las zonas luminosas de la cara del gato quemadas. 
Este año hemos visto algún gran terminal en fotografía diurna pero que sucumbía a la realidad de un sensor con muchos megapíxeles y poco tamaño en cuando la luz escaseaba. Huawei y Google han tomado cartas en el asunto y la cámara de este Nexus 6P presenta cifras para ofrecer un comportamiento decente con poca luz. Pero no ha sido exactamente así. 
Las fotos de noche han sido inconsistentes y en algunas circunstancias se derrumbaba con un excesivo ruido o peor, un tratamiento que eliminaba cualquier detalle. Esto nos ha ocurrido especialmente cuando era escena nocturna pero con focos de luz. Sin embargo, cuando el sensor de más tamaño y la luminosidad de la lente podían entrar en juego "sin despistes", los resultados han sido más satisfactorios. 
Y pasamos ahora a la interfaz. Aquí la simplicidad roza lo absurdo y no hay manera de controlar apenas ningún aspecto de la cámara del Nexus 6P , lo que choca  con el potencial tan grande de la cámara. Es cierto que para apuntar y disparar es efectiva y muy rápida en todo el proceso, pero al nivel que estamos en la actualidad, queremos mucho más. 
En esa interfaz simplista nos encontramos con la moda del HDR en todo momento. Y hay que decir que Huawei lo ha implementado bastante bien. No llega al nivel del HDR de Samsung, pero es recomendable mantener el modo HDR automático en todo momento porque nos va a conseguir mejores fotos en general que sin él. 
La ráfaga es bastante rápida y permite congelar algunos momentos en escenas de acción. Podemos seleccionar que el propio Nexus 6P nos escoja la mejor instantánea de la serie o que nos las guarde todas para poder decidir nosotros cuál nos quedamos.  Pero hay algo mejor: la aplicación te genera un gif automático bastante simpático de la acción que se ve en la galería como tal pero también se exporta automáticamente al ordenador como imagen animada. 
La cámara se puede abrir directamente incluso con el terminal bloquedo con un doble clic en el botón de encendido. Una vez que asimilas esta posibilidad es una manera muy cómoda de usar el Nexus 6P para hacer fotografías de forma más ágil y sin pensar en desbloquear y abrir la aplicación de cámara. 
En modo vídeo los resultados ya no nos parecen tan buenos como con rivales, y echamos de menos especialmente la estabilización óptica. Tanto el vídeo 4K como la cámara lenta hasta a 240 fps nos ha dejado bastante fríos.
 


            Ver galería completa » Nexus 6P cámara (23 fotos)
    

























































































































 window.addEventListener('load', function(){
    $("#carousel-1").elastislide();
 });


Ya es hora de olvidarse de los Nexus como terminales asequibles y con unas prestaciones por encima de su gama. Ahora hay que verlos como genuinos modelos bajo la marca Google. Como si de un fabricante más se tratara. Con el Nexus 6P en conjunto a Google y Huawei les ha salido un terminal de lo más redondo y en un rango de precios acorde con la gama alta de las grandes marcas. 
La pantalla está entre las mejores del año, no hay concesiones en potencia o batería y el diseño es de lo mejor de siempre en el mundo de los smartphones. También el lector de huellas funciona de lujo. Y mucho ojo con la carga vía USB tipo C porque además de la ventaja de la carga rápida y su reversibilidad, más de una vez, si no compras más cables, te encontrarás sin posibilidad de carga.
En cuanto a la cámara, responde en la mayoría de los casos de forma excelente cuando hay luz, no tanto de noche, pero ya ha alcanzado un nivel como para que no tenga que afectar negativamente a decidirse por un Nexus u otro gama alta. La diferencia hoy en día la marca Android y tener la última versión siempre disponible. Ahí por ahora no tiene rival, a lo que hay que sumar que es un excelente terminal y a precio de mercado. 
9,1
También te puede gustar
+ en Xataka
Tecnología
Estilo de vida
Motor
Ocio
Economía
Latinoamérica
Participamos en
