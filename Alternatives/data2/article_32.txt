http://www.xataka.com/moviles/te-parece-buena-la-camara-en-los-iphones-hay-800-personas-dandole-forma-en-apple
Gadgets y tecnología: últimas tecnologías en electrónica de consumo - XATAKA






Buscar »

Google+
@kotecinho
Editor senior en Xataka
Así es cómo se consigue convertir un tablet en un equipo de sonido 5.1
Los tablets tienen un problema serio con el sonido. ¿Por qué suena tan enlatado? Así es la solución de Huawei.

Cada vez que aparece un nuevo teléfono firmado por Apple, nosotros intentamos dedicarle el espacio que se merece a su cámara, lo hicimos con el iPhone 6s, también con el modelo anterior, o con el iPhone 5s. Siempre es interesante conocer más cosas sobre este apartado, sobre todo si la compañía no se prodiga en dar datos sobre sus creaciones, ni durante la presentación, ni tiempo después.
Para que nos hagamos una idea de lo importante que es el desarrollo de una cámara para Apple, nos informan de que hay 800 personas trabajando en el departamento que se encarga de gestar ese módulo. Me gustaría saber cuántos ingenieros y especialistas hay en firmas dedicadas únicamente a crear cámaras, pero me parece que no debe contar con un número tan alto.
¿Cómo nos enteramos de este dato? Pues gracias a una entrevista que Tim Cook y su equipo principal - Angela Ahrendts, Eddy Cue, Jony Ive, Phil Schiller - han concedido al programa de la CBS ’60 minutes’, con Charlie Rose como presentador. Dejando a un lado detalles concretos como los de las cámaras, tampoco hay mucho donde rascar en la entrevista.
Volviendo al tema que nos ocupa, comentar que al mando del inmenso departamento que crea las cámaras para iPhones está Grahan Towndsend. Muchos ingenieros y desarrolladores que trabajan al unísono para dar forma a un módulo con más de 200 piezas diferentes. También son los encargados de gestar un software reconocido por todos: sencillo, capaz, y sin demasiadas lagunas.
En la incursión dentro de los laboratorios de Apple, también hay ocasión para demostrar cómo Apple simula todo tipo de condiciones para probar las cámaras - de plena luz del día a muy poca luz -, y en consecuencia calibrarlas. Nos enseñan las partes y el sistema de estabilización - basado en 'microsuspensión' - en el siguiente vídeo:

En resumidas cuentas, en la entrevista se habla con Tim Cook sobre la dirección de una empresa gigante y moderna, con los retos que se enfrenta cada día, o los temas que siempre rodean a la compañía: críticas por su fabricación en China, la inmensa hucha de ingresos que han conseguido recaudar en los últimos años - los impuestos en Irlanda
 -, en lo que piensan gastarlo, o la posible creación de un coche. 
Si estáis interesados en la entrevista completa, medios como TechCrunch tienen la transcripción completa, además podemos compartir contenido adicional de la propia CBS:
View More: 60 Minutes News|Live News|More News Videos.cbs-link {color:#4B5054;text-decoration:none; font: normal 12px Arial;}.cbs-link:hover {color:#A7COFF;text-decoration:none; font: normal 12px Arial;}.cbs-pipe {color:#303435;padding: 0 2px;}.cbs-resources {height:24px; background-color:#000; padding: 0 0 0 8px; width: 612px;}.cbs-more {font: normal 12px Arial; color: #4B5054; padding-right:2px;}
View More: 60 Minutes News|Live News|More News Videos.cbs-link {color:#4B5054;text-decoration:none; font: normal 12px Arial;}.cbs-link:hover {color:#A7COFF;text-decoration:none; font: normal 12px Arial;}.cbs-pipe {color:#303435;padding: 0 2px;}.cbs-resources {height:24px; background-color:#000; padding: 0 0 0 8px; width: 612px;}.cbs-more {font: normal 12px Arial; color: #4B5054; padding-right:2px;}
View More: 60 Rewind News|Live News|More News Videos.cbs-link {color:#4B5054;text-decoration:none; font: normal 12px Arial;}.cbs-link:hover {color:#A7COFF;text-decoration:none; font: normal 12px Arial;}.cbs-pipe {color:#303435;padding: 0 2px;}.cbs-resources {height:24px; background-color:#000; padding: 0 0 0 8px; width: 612px;}.cbs-more {font: normal 12px Arial; color: #4B5054; padding-right:2px;}
En Xataka | Los mejores smartphones con cámara de 2015 a prueba: comparativa fotográfica
También te puede gustar
Recomendado en Magnet
+ en Xataka
Tecnología
Estilo de vida
Motor
Ocio
Economía
Latinoamérica
Participamos en
