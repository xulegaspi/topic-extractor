http://www.xataka.com/moviles/nexus-5x-primeras-impresiones-el-nexus-mas-querido-vuelve-por-la-puerta-grande
Gadgets y tecnología: últimas tecnologías en electrónica de consumo - XATAKA






Buscar »

Google+
@jcgonzalezgo
Editor senior en Xataka
Así es cómo se consigue convertir un tablet en un equipo de sonido 5.1
Los tablets tienen un problema serio con el sonido. ¿Por qué suena tan enlatado? Así es la solución de Huawei.

El Nexus 5 es, sin lugar a duda, el modelo más popular de esta familia de terminales hechos mano a mano entre Google y un fabricante. Araceli de la Fuente, de LG España, lo recordaba en la presentación del Nexus 5X esta mañana. Los modelos que han hecho los coreanos han tenido muy buena acogida, es verdad.
Nexus 5 marcó un antes y un después al ser el primer gama alta a precio comedido. Un gran terminal, con carencias, que cautivó a los amantes de Android. La estrategia no se repitió el año pasado con el Nexus 6 y este año el 5X apuntaba a recuperar ese trono. A ser el Nexus barato pero con buenas especificaciones. Lo han conseguido, a medias. Hoy hemos podido probarlo en Madrid y aunque nos ha gustado, ha habido algunas cosas que no nos han convencido.

El Nexus 5 fue mi móvil principal durante una buena temporada y guardo un buen recuerdo de él. Tenía carencias, sí, pero era un terminal soberbio. Cuando lo he cogido esta mañana la sensación ha sido la misma que hace dos años: el mismo tacto sedoso, para lo bueno y para lo malo. Los coreanos han decidido volver a apostar por el plástico y teniendo en cuenta su precio, es un material que nos sabe a poco.
Lo bueno de usar policarbonato es que este 5X es ligero, el tamaño y la forma es idóneo para cogerlo con una mano pero algunos acabados no están a la altura de un gama alta y hay terminales que en un escalón por debajo tienen mejor acabado, especialmente en los botones del lateral o el diseño de los altavoces. 
Es un terminal sobrio, funcional, como la mayoría de los Nexus. La cámara, en la parte trasera, sobresale muy poco y apenas se nota joroba. El anillo del lector es un acierto ya que nos ayuda a localizar este componente cuando cogemos el móvil y lo palpamos por la parte trasera.
En la pantalla LG ha optado por un panel IPS LCD de calidad. No despunta es ninguna dirección pero cumple muy bien: no se aprecia ninguna desviación hacia ningún tono, tiene mucho brillo (aunque no es de los que más nits tiene este año) y el espacio de color es amplio. El cristal es algo grueso y produce reflejos algo molestos con luz natural. Veremos en el día a día pero parece que con la luminancia al máximo no habrá problemas.
Qualcomm Snapdragon 808 en el procesador. En los tests sintéticos puntúa alto, es un SoC de gama alta, pero está un escalón por debajo de su hermano 810 y el Exynos 7. No es algo que debería de preocuparnos porque lo que hemos visto hoy nos ha gustado: rápido, fluido, abre bien cualquier aplicación. Es un Nexus y aquí no hay sorpresas, está a la altura del resto de la familia.
Veremos en nuestro futuro análisis si en el día a día es suficiente para aguantar una carga de trabajo exigente. A priori debería ser competitivo, la duda está en la autonomía donde Marhsmallow tendrá que demostrar que con Doze es capaz de arreglar el desastre de gestión que hizo Lollipop hace un año.
Lollipop trajo muchas novedades a Android pero su esfuerzo no fue suficiente. Fue un sistema con carencias que se fueron corrigiendo con actualizaciones posteriores, revisiones que no han llegado a todos los usuarios. Marshmallow viene a poner un poco de orden y optimizar todo eso, además de traer unos cuantos trucos nuevos.
La experiencia en el Nexus 5X es excepcional y demuestra por qué esta familia de terminales debe ser la referencia para cualquier fabricante de móviles. Los cambios frente a Lollipop son muy sutiles a primera vista: animaciones más fluidas, pequeñas modificaciones en la interfaz para hacer más accesibles algunos atajos…
Como en generaciones anteriores, es un móvil perfecto para quien quiera Android tal y como lo entiende Google. Si venimos de Lollipop tardaremos un poco en ver algunos detalles pero poco a poco los iremos descubriendo. En el tiempo que he estado con él he ido comparando con un Moto X Style en una especie de juego por encontrar las diferencias.
El lector de huellas incorporado es igual de bueno como el que encontraríamos en otros terminal. Con unos cuantos móviles con lector ya en el mercado, podemos tener la tranquilidad de que cualquiera de estos teléfonos va a ir bien. Para desbloquearlo, tan solo tenemos que apoyar la yema del dedo que tengamos configurado, sin necesidad de pulsar un botón.
Sobre la cámara es pronto para opinar, las condiciones que había en el evento no eran las mejores para arrojar unas primeras concusiones pero vemos que la calidad será suficiente para la mayoría de las circunstancias. Sensor luminoso, rango dinámico correcto y velocidad de enfoque en la media.
El software de procesado, en interiores, no produce mucho ruido y el HDR ayuda a que en escenas de mucho contraste el rango dinámico se estire un poco y podamos ver más detalles en las sombras. De nuevo, nos toca esperar al análisis para sacar conclusiones más reposadas y fundamentadas.
Muchos esperábamos que el Nexus 5X fuera algo más asequible, que volviera a ser ese móvil potente para todos. Los 379 dólares (sin impuestos) para Estados Unidos eran un mero espejismo. 479 euros será su precio de salida, cantidad que cuesta un poco digerir pero que no es del todo alta, aunque por esa cifra hay otra alternativas muy interesantes. 
Al preguntar por la diferencia de precio entre Estados Unidos y Europa (hace tiempo hicimos las cuentas) LG afirma que el precio se trata de “una estrategia global de la compañía”. No sé muy bien a qué se refieren los coreanos con eso pero como respuesta es mejor que darme largas. Me encojo de hombros, no es una diferencia muy grande pero ahí está.
Las primeras impresiones con el Nexus 5X son positivas. Sabíamos de antemano, gracias a una filtración, que los precios no iban a ser tan atractivos como en el modelo de 2013. Aún con eso, nos queda un terminal potente, con prestaciones redondas pero que a simple vista no despunta en nada. Ese rol, el de ser excelente, parece que será para un Nexus 6P que viene pisando fuerte.
Es un terminal atractivo y como Nexus “barato” para este año será una opción a tener en cuenta. Está en una línea incómoda, entre la amplia gama media y la élite de los más potentes de este año. Una zona donde resulta difícil hacerse valer pero Nexus y su filosofía de móvil Google es atractiva. Veremos si es suficiente o no para triunfar.
También te puede gustar
Recomendado en Magnet
+ en Xataka
Tecnología
Estilo de vida
Motor
Ocio
Economía
Latinoamérica
Participamos en
