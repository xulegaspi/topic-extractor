http://www.xataka.com/moviles/nexus-6p-la-gama-alta-economica-segun-google-y-huawei
Gadgets y tecnología: últimas tecnologías en electrónica de consumo - XATAKA






Buscar »

Editor en Xataka
Así es cómo se consigue convertir un tablet en un equipo de sonido 5.1
Los tablets tienen un problema serio con el sonido. ¿Por qué suena tan enlatado? Así es la solución de Huawei.

Hoy es el día en el que los meses de rumores llegan a su fin, y en el que Google mueve ficha de una vez presentándole al mundo su nueva horneada de dispositivos. Por ser un intento de volver a la época dorada de los Nexus, el 5X es el que se ha llevado la mayoría de las miradas, pero la verdadera apuesta por la gama alta de la empresa del buscador tiene el nombre del Nexus 6P.
Google y Huawei se han dado la mano para intentar crear competidor digno con el que hacer ruido en la gama alta. Con él sencillamente han creado un phablet con uno de los mejores hardwares que hoy se puede montar, y lo han intentado poner a disposición del gran público a un precio inferior al que piden otros fabricantes en la gama alta.
Mucho se ha estado escribiendo sobre el diseño de este móvil y sobre el aspecto de periscopio que parecía darle el saliente en el que se sitúan su cámara de 12,3 megapíxeles, el doble Flash y el sensor láser. Pero al final no ha sido para tanto, y cuando Google lo ha enseñado no parecía que tuviera una curvatura tan pronunciada. Por cierto, la cámara utiliza una tecnología de píxeles de 1.55 μm, un concepto similar al de los ultrapíxeles de HTC.
Si hablamos de los materiales, nos encontramos ante un phablet con un cuerpo unibody acabado en aluminio, y que estará disponible en colores negro, blanco, aluminio y dorado. En la parte trasera también destaca la existencia de un sensor de huellas perfecto para estrenar Android 6.0, ya que entre sus novedades nos encontramos con un mejor soporte para estos lectores.
Mide 159,4 x 77,8 x 7,3 milímetros y pesa 178 gramosTeniendo en cuenta que su pantalla sube hasta las 5,7 pulgadas no nos debe extrañar que sus dimensiones sean bastante grandes. Las medidas de este Nexus 6P son de 159,4 x 77,8 x 7,3 milímetros, y su peso se situa en 178 gramos. La parte positiva es que en su interior a Google le ha cabido una batería de 3.450 mAh.
Mide 159,4 x 77,8 x 7,3 milímetros y pesa 178 gramos
En cuanto a la parte frontal, en ella nos encontraremos con una cámara de ocho megapíxeles. También veremos situados tanto en la parte superior como en la inferior sus dos altavoces frontales estéreo, y todo ello con la ya mencionada pantalla de 5,7 pulgadas con una resolución QHD de 1440 píxeles.


Al contrario que su hermano menor, este móvil sí que vendrá equipado con prácticamente el mejor hardware disponible en este momento. Eso se traduce en un procesador Qualcomm Snapdragon 810 v2.1, 3 gigabytes de memoria RAM y unas configuraciones de almacenamiento interno de 32, 64 e incluso 128 gigas no ampliables.
Se irá actualizando con la última versión de Android durante por lo menos dos añosComo en todos los Nexus, el gran aliciente que Google le quiere añadir a estas especificaciones es el de ofrecer una experiencia limpia, con un Android libre de capas de personalización y cualquier aplicación más allá que las instaladas por la empresa del buscador. Eso sin contar con que el móvil será el primero en estrenar Android 6.0 Marshmallow y que durante dos años seguirá recibiendo actualizaciones del sistema operativo en el mismo momento en el que se lancen.
  
Reseñables también son algunas ausencias que nos encontramos en esta nueva horneada de dispositivos Nexus, ya que ni tendrán un sistema de carga inalámbrica como el que llevan montando todos los móviles de Google desde el 2012, ni tampoco podremos beneficiarnos de un estabilizador óptico de imagen (OIS) a la hora de sacar nuestras fotos.
Pero estas ausencias tampoco consiguen empañar del todo la apuesta por un hardware puntero por parte de Google y Huawei, y la hoja de especificaciones de su Nexus 6P viene rematada con un puerto de carga USB 3.1 Tipo C con carga rápida Quick Charge 2.0 y los clásicos Bluetooth 4.0, NFC y conectividad 4G.
Precio y Disponibilidad
  
El Nexus 6P estará disponible en preventa a partir de hoy en las Google Store de Estados Unidos, Reino Unido, Irlanda y Japón, y empezarán a enviarse a los compradores a finales de octubre. Los móviles también irán llegando al resto de países, pero habrá que esperar algunas semanas para que estén disponibles.
En cuanto a los precios, pese a lo que veis en la captura el Nexus 6P estará disponible por 649 euros para la versión de 32 GB de almacenamiento, 699 para la de 64 GB y 749 euros para la de 128 gigas. Se ve que esta gama alta sólo será económica para algunos países.
En Xataka | Nexus 5X: ya está aquí el esperado relevo del Nexus 5


Al contrario que su hermano menor, este móvil sí que vendrá equipado con prácticamente el mejor hardware disponible en este momento. Eso se traduce en un procesador Qualcomm Snapdragon 810 v2.1, 3 gigabytes de memoria RAM y unas configuraciones de almacenamiento interno de 32, 64 e incluso 128 gigas no ampliables.
Se irá actualizando con la última versión de Android durante por lo menos dos añosComo en todos los Nexus, el gran aliciente que Google le quiere añadir a estas especificaciones es el de ofrecer una experiencia limpia, con un Android libre de capas de personalización y cualquier aplicación más allá que las instaladas por la empresa del buscador. Eso sin contar con que el móvil será el primero en estrenar Android 6.0 Marshmallow y que durante dos años seguirá recibiendo actualizaciones del sistema operativo en el mismo momento en el que se lancen.
Se irá actualizando con la última versión de Android durante por lo menos dos años
Reseñables también son algunas ausencias que nos encontramos en esta nueva horneada de dispositivos Nexus, ya que ni tendrán un sistema de carga inalámbrica como el que llevan montando todos los móviles de Google desde el 2012, ni tampoco podremos beneficiarnos de un estabilizador óptico de imagen (OIS) a la hora de sacar nuestras fotos.
Pero estas ausencias tampoco consiguen empañar del todo la apuesta por un hardware puntero por parte de Google y Huawei, y la hoja de especificaciones de su Nexus 6P viene rematada con un puerto de carga USB 3.1 Tipo C con carga rápida Quick Charge 2.0 y los clásicos Bluetooth 4.0, NFC y conectividad 4G.
El Nexus 6P estará disponible en preventa a partir de hoy en las Google Store de Estados Unidos, Reino Unido, Irlanda y Japón, y empezarán a enviarse a los compradores a finales de octubre. Los móviles también irán llegando al resto de países, pero habrá que esperar algunas semanas para que estén disponibles.
En cuanto a los precios, pese a lo que veis en la captura el Nexus 6P estará disponible por 649 euros para la versión de 32 GB de almacenamiento, 699 para la de 64 GB y 749 euros para la de 128 gigas. Se ve que esta gama alta sólo será económica para algunos países.
En Xataka | Nexus 5X: ya está aquí el esperado relevo del Nexus 5
También te puede gustar
Recomendado en Magnet
+ en Xataka
Tecnología
Estilo de vida
Motor
Ocio
Economía
Latinoamérica
Participamos en
