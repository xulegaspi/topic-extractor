http://www.xataka.com/moviles/qualcomm-snapdragon-810-el-corazon-de-los-que-quieren-ser-los-mejores-smartphones-de-2015
Gadgets y tecnología: últimas tecnologías en electrónica de consumo - XATAKA






Buscar »

Google+
@whiskito
Así es cómo se consigue convertir un tablet en un equipo de sonido 5.1
Los tablets tienen un problema serio con el sonido. ¿Por qué suena tan enlatado? Así es la solución de Huawei.


Los Snapdragon 801 son el presente y los Snapdragon 805 el futuro más inmediato. Sin embargo, si miramos un poco más lejos en la línea temporal también veremos otro miembro del mercado de los SoC móviles: los Qualcomm Snapdragon 810.
Un conjunto de chips cuyo lanzamiento está previsto se realice a lo largo de 2015 y que volverán a girar la rueda de la evolución tecnológica de estos componentes de hardware. Snapdragon 810 serán los SoC de los grandes teléfonos de 2015, con características mejoradas respecto de lo conocido en la actualidad y con soporte para tecnologías que están aún por llegar.
Qualcomm Snapdragon 810 serán los primeros con arquitectura big.LITTLE de entre los destinados por Qualcomm a la gama más alta de productos. Una transición importante con la que ARM busca la eficiencia energética y de rendimiento: cuatro núcleos de relativo bajo rendimiento para la mayoría de tareas, y otros cuatro de alto potencial cuando se requiera una mayor capacidad de proceso.
Los Snapdragon 810 serán por supuesto de 64 bits y formarán parte de esa transición lenta que durará unas cuantas generaciones. Como el resto de chips Snapdragon con esta longitud de memoria, Qualcomm utilizará los núcleos estándar Cortex: cuatro Cortex-A53 y otros cuatro Cortex-A57, cuyas frecuencias aún no han sido anunciadas.
Un punto importante es que estamos ante núcleos Cortex fabricados en 20 nanómetros, suponiendo un importante salto cualitativo respecto de los actuales 28 nanómetros de las arquitecturas más avanzadas. Previsiblemente serán de los primeros SoC bajo este proceso de fabricación que, como siempre, permitirá reducir el consumo energético y los tamaños de los chips a la vez que mejoraría sensiblemente el rendimiento aportado. También tendrán soporte para otras tecnologías como LPDDR4 o LTE Cat 6/7, al menos el modelo con modem actualmente presentado, el MSM8994.
Cuando analizamos la familia de los Snapdragon 805 nos paramos a estudiar con mayor detenimiento a la Adreno 420, su GPU. La primera de una nueva familia - las series Adreno 400 - que tendrá un nuevo modelo en la generación de los Snapdragon de la que hablamos hoy.

Los Snapdragon 810 utilizarán una GPU Adreno 430. Una decena más que significa mantener buena parte de las características y tecnologías de la Adreno 420. Sin embargo, las primeras estimaciones oficiales afirman que Adreno 430 será un 30% superior en GPGPU a Adreno 420. Una ganancia muy significativa en un relevo generacional como este, aunque como siempre son datos que tendrán que ser refutados con pruebas de las versiones finales del hardware.
Si buscamos la comparación de Adreno 430 con otros chips nos sorprenderemos al comprobar que aseguran poder aportar un 80% más rendimiento que Adreno 330, la GPU de los Snapdragon 800 y 801 comunes en las gamas más altas del mercado actual. Llegar a casi doblar el potencial GPGPU en sólo un par de generaciones (un tiempo de unos dos años) es una cifra realmente impresionante que no sólo beneficia a los usuarios, también nos da que pensar acerca del rápido crecimiento de un mercado como es el del hardware de nuestros gadgets.
En otro orden de cosas relacionadas con el aspecto gráfico, todas estas mejoras implementadas en los Snapdragon 810 tienen un claro objetivo: la resolución 4K, tanto en grabación como en visualización. Soportarán vídeo 4K a 60 imágenes por segundo gracias, en parte, al uso de un codificador y decodificador del H.265, un estándar de codificación de vídeo muy habitual en el contenido en alta definición y que ganará terreno a medida que el UHD vaya entrando en el ambiente doméstico.
Los Qualcomm Snapdragon 810 no son un producto que vayamos a ver en el corto plazo, ni mucho menos. Con el Snapdragon 805 en el mercado pero únicamente disponible (por ahora) en un Galaxy S5 LTE-A en Corea, queda claro que aún tienen mucho recorrido que realizar en el mercado.
800, 801, 805... y después 810. El primero ya es casi historia, el segundo es el presente, el tercero el futuro inmediato (será el protagonista de los nuevos buques insignia que se presentarán durante los próximos meses) y los Snapdragon 810 como el futuro a un plazo algo mayor.
¿De cuanto tiempo estamos hablando? Mucho en términos de tiempo en tecnología, pero no tanto en la realidad. El aterrizaje de los Snapdragon 810 está programado para 2015, con producción de las primeras unidades durante los primeros meses del año y disponibilidad en el mercado final de cara a la segunda mitad del año. Respecto de las características más esperadas para formar parte de estos nuevos dispositivos, todos coincidiremos en que ofrecerán mejoras relacionadas con la resolución: no sólo 2K, si no también pantallas 4K así como por supuesto cámaras capaces de reproducir y grabar este tipo de contenido.

También te puede gustar
Recomendado en Magnet
+ en Xataka
Tecnología
Estilo de vida
Motor
Ocio
Economía
Latinoamérica
Participamos en
