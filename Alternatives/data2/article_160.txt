http://www.xataka.com/moviles/fujitsu-acerara-sus-smartphones-a-la-tercera-edad-en-europa-y-estados-unidos
Gadgets y tecnología: últimas tecnologías en electrónica de consumo - XATAKA






Buscar »

Google+
@jcgonzalezgo
Editor senior en Xataka
Así es cómo se consigue convertir un tablet en un equipo de sonido 5.1
Los tablets tienen un problema serio con el sonido. ¿Por qué suena tan enlatado? Así es la solución de Huawei.


Smartphones y simplicidad, a la hora de usarlos, parecen dos conceptos cada vez más lejanos. Es cierto que muchos de nosotros ya nos hemos hecho a sus interfaces pero para otros sigue siendo un suplicio navegar entre opciones y salir de las dos o tres apps que más usan.
En ese esfuerzo por simplificar, y por llegar a la gente mayor, Fujitsu ha anunciado que lanzará en Europa y Estados Unidos su gama de smartphones Raku Raku destinada a la tercera edad y, como no, a todos aquellos que busquen un teléfono sencillo de usar.

Raku Raku, para quien no la conozca, es una línea de teléfonos móviles desarrollada por Fujitsu y distribuida en Japón por el operador NTT Docomo. Una gama orientada a simplificar el uso y que cualquiera pueda tenerlo y emplearlo independientemente de su habilidad.
El objetivo es claro: la gente mayor. Este nicho, salvo excepciones, todavía no ha logrado dominar este tipo de dispositivos. Ya sea por su interfaz o por su diseño todavía sigue siendo más útil un teléfono móvil convencional o los que sacan fabricantes como Emporia.
También está la cuestión de usabilidad, quizá haya quienes duden de que la gente necesite tantos servicios pero eso ya es algo más subjetivo. En cualquier caso, estos smartphones que funcionan con Android llegarán durante el 2013. Eso sí, esperemos que su aventura dure algo más que la de Panasonic en Europa, que fue bastante corta.
Los que esperéis más teléfonos de Fujitsu, como la gama Arrow, sentimos deciros que de momento Fujitsu no pretende introducirlos al mercado. Veremos qué tal funciona y cuál será la estrategia para posicionarse en el mercado que en el caso de España las operadoras juega un papel importante para darse a conocer al gran público.
Vía | Asahi Shimbum
También te puede gustar
Recomendado en Magnet
+ en Xataka
Tecnología
Estilo de vida
Motor
Ocio
Economía
Latinoamérica
Participamos en
