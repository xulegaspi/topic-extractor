http://www.xataka.com/moviles/nos-hemos-cansado-de-xiaomi
Gadgets y tecnología: últimas tecnologías en electrónica de consumo - XATAKA






Buscar »

Google+
@javipas
Editor senior en Xataka
Así es cómo se consigue convertir un tablet en un equipo de sonido 5.1
Los tablets tienen un problema serio con el sonido. ¿Por qué suena tan enlatado? Así es la solución de Huawei.

El año pasado Xiaomi destrozó su récord de ventas: en enero anunciaba 61,1 millones de terminales vendidos en 2014 frente a los 18,7 millones de 2013, y su fundador, Lei Jun, prometía un "más y mejor" para 2015. Auguraba de hecho unas cifras que podrían llegar a los 100 millones de terminales en estos 12 meses. 
Aquellas estimaciones parecen haber sido demasiado optimistas. Durante el primer semestre de año vendieron 35 millones de terminales, una cantidad destacable, desde luego, pero no demasiado en línea con los objetivos de los que hablaba Jun en ese pronóstico a principios de año. Diversos analistas creen que la empresa tendrá un año destacable, pero no tanto como el fabricante había previsto. ¿Cuáles son las razones? ¿Estamos aburridos de Xiaomi?
El año empezaba fuerte con la presentación de los Xiaomi Mi Note y Mi Note Pro, que se convertían en dignos rivales en muchos apartados en la gama alta, pero que pronto se verían perjudicados por al menos tres factores clave. 
El primero, la avalancha de lanzamientos de la competencia, que demostró estar a un gran nivel en esos primeros meses de 2015. El segundo, el hecho de que aquello que llamaba tanto la atención en aquella Xiaomi original -móviles con grandes prestaciones a precios de chollo- ya no era parecía hacerlo tanto en los lanzamientos. 
Y tercero, la lentitud del plan de expansión y el buen hacer de sus rivales en China, tanto en fabricantes locales -Huawei o OnePlus- como en marcas internacionales que dieron el do de pecho, como Samsung y -más recientemente- Apple. De hecho nuestros compañeros de Xataka Móvil también analizaban estos problemas hace unos días y hablaban de otro elemento singular de esa situación: Xiaomi está más dispersa. 
Los datos de TrendForce difieren mucho de las previsiones de Xiaomi, y sitúan las ventas de smartphones en 70 millones de unidades, muy lejos de las que por ejemplo se prevén para Huawei, que serían de 110 millones de unidades para 2015. La posición global de esta última es desde luego determinante en esas cifras de ventas. La cosa no se queda ahí: Canalys informaba recientemente de que el tercer trimestre de 2015 había sido más bajo en ventas para Xiaomi que el mismo periodo de 2014, algo que supondría el primer descenso de esta empresa en este segmento.
Lo cierto es que aquella empresa que nos dejaba boquiabiertos con terminales de gamas medias-altas a precios de risa comenzó hace meses ha diversificar en todo tipo de ámbitos en los que quería tratar de llegar a las mismas propuestas en las que la relación precio/prestaciones era sorprendente. 

Lo logró con muchos de esos productos, pero esos lanzamientos parecen haber comenzado a tener un impacto negativo en su división de dispositivos móviles, que es menos llamativa de lo que era cuando Xiaomi inició su andadura. Los lanzamientos de este año en este terreno han sido destacables, pero no han logrado mantener el interés de dispositivos anteriores. 
Es el caso del Xiaomi Mi Note que costaba 320 euros (450 por el Pro), del Xiaomi Mi 4c que ronda los 220 euros o del Redmi Note 2 que se ofrece en tres versiones distintas por precios que van de los 110 a los 140 euros. 
A ellos se suman los que se espera llegarán en las próximas semanas. En primer lugar, el Xiaomi Mi5, heredero de esa familia con la que el fabricante saltó a la fama. Y el segundo, un Redmi Note 2 Pro en el que se espera encontremos un lector de huella dactilar entre otras mejoras. 
Todos ellos siguen destacando por esas grandes prestaciones y ese precio comedido, pero aquí es donde incide el problema de la expansión internacional de Xiaomi, que hace que para muchos usuarios el no tener acceso a un canal de venta más directo y a una garantía en la que poder confiar resulte difícil acudir a la oferta de este fabricante. Esos planes se está limitando a regiones muy específicas, y de hecho Xiaomi acaba de anunciar el inicio de su expansión en África.
De hecho los precios oficiales en China salen perjudicados cuando uno trata de encontrarlos en tiendas que hacen importación de esos teléfonos. Muchas tiendas online en nuestro idioma se dedican a proporcionar estos modelos con dos años de garantía -muchas proporcionan su propio servicio técnico, no el oficial de Xiaomi que no existe en nuestro país- pero por ejemplo el precio se resiente. El Xiaomi Mi Note "normal" de 5,7 pulgadas, resolución 1080p  y basado en MIUI v6 sale por 399 euros IVA incluido en algunas de ellas, y por ese precio empezamos a ver terminales muy atractivos de otras firmas que pueden competir en muchas de sus prestaciones. 
El problema de la expansión internacional -Hugo Barra afirmaba que sería lenta- es probablemente el que más debería preocupar a Xiaomi. Otras marcas han logrado irse posicionando para vender fuera de China de forma oficial, y están ganando esa carrera de la popularidad. Incluso OnePlus con su polémico sistema de invitaciones está logrando cuajar en un mercado ultracompetitivo, y eso sin duda está perjudicando a la startup que demostró que otro modelo era posible. 

Sin embargo hay otros factores a los que hay que atender, y uno de ellos podría ser la diversificación del mercado a la que aludíamos anteriormente. Xiaomi, que originalmente se dedicaba a los smartphones, ahora toca otros muchos palos. Tiene televisores, baterías externas, cámaras de acción, pulseras de actividad, clones del Segway, routers, mandos de videojuegos Bluetooth, auriculares e incluso futuros portátiles.
En prácticamente todos los casos tenemos ante nosotros un despliegue en el que de nuevo la relación precio/prestaciones es especialmente destacable, y de hecho algunos de esos accesorios sí se han beneficiado de un plan de expansión que comienza a llegar a Estados Unidos y Europa, pero sin que los smartphones hagan acto de presencia. Y lo que muchos nos preguntamos es si toda esa ambición no les estará perjudicando a la hora de desarrollar nuevos smartphone más ambiciosos o diferenciales. 
Pero quizás eso no es lo que quiere Xiaomi, que a pesar de apartados destacables como el de su ROM MIUI basada en Android no se ha caracterizado por innovar, sino por adoptar diseños y prestaciones introducidas por otras firmas para ofrecerlas con diseños y precios muy destacables. La pregunta es: ¿nos hemos cansado de Xiaomi? ¿Recomendáis estos modelos tanto como podíais hacerlo en el pasado?
En Xataka | El fenómeno de Xiaomi explicado en 13 cifras
También te puede gustar
+ en Xataka
Tecnología
Estilo de vida
Motor
Ocio
Economía
Latinoamérica
Participamos en
