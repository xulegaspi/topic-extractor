http://www.xataka.com/moviles/el-zte-geek-integrara-el-soc-nvidia-tegra-4-y-pantalla-1080p
Gadgets y tecnología: últimas tecnologías en electrónica de consumo - XATAKA






Buscar »

Google+
@javipas
Editor senior en Xataka
Así es cómo se consigue convertir un tablet en un equipo de sonido 5.1
Los tablets tienen un problema serio con el sonido. ¿Por qué suena tan enlatado? Así es la solución de Huawei.


Aunque ya en el mes de abril aparecieron rumores sobre el ZTE Geek U988S, en aquel momento se pensaba que éste futuro terminal integraría el procesador Intel CloverTrail+, pero ahora sabemos que el SoC elegido será el NVIDIA Tegra 4, convirtiéndose así en el primer terminal con este chip.
El futuro ZTE Geek además incluirá una pantalla de 5 pulgadas y con resolución 1080p, convirtiéndose así en uno de los grandes protagonistas de la próxima hornada de smartphones. Algunos de ellos apostarán por los Snapdragon 800, los grandes rivales de los Tegra 4 en esa generación.

Los rumores apuntan además a que este ZTE Geek llegará con 2 GB de RAM, una capacidad desconocida de almacenamiento interno —pero con presencia de ranura microSD—, y una cámara con un sensor de 13 Mpíxeles. 
Los datos preliminares apuntan a que este terminal estará únicamente disponible a través de China Mobile, la operadora móvil del gigante asiático, pero es de esperar que el atractivo ZTE Geek dé el salto también a mercados internacionales.
Vía | AndroidCentral
En Xataka | Qualcomm Snapdragon 800 a examen
También te puede gustar
Recomendado en Magnet
+ en Xataka
Tecnología
Estilo de vida
Motor
Ocio
Economía
Latinoamérica
Participamos en
