http://www.xataka.com/moviles/el-negocio-movil-de-samsung-tiene-su-talon-de-aquiles-en-el-software-segun-reuters#to-comments
Gadgets y tecnología: últimas tecnologías en electrónica de consumo - XATAKA






Buscar »

Google+
@kotecinho
Editor senior en Xataka
Así es cómo se consigue convertir un tablet en un equipo de sonido 5.1
Los tablets tienen un problema serio con el sonido. ¿Por qué suena tan enlatado? Así es la solución de Huawei.

Samsung es la que manda en el mercado móvil, a pesar de tener altibajos en los periodos, y potentes cambios en la estrategia a seguir con un catálogo tan grande de productos. En los últimos tiempos los estamos viendo mejorar la calidad de componentes, también colocar más elementos hardware de manufactura propia, pero en el software nunca han sido la referencia.
Toda la vida vistiendo a Android con TouchWiz, desde el más pequeñín de los terminales Android, hasta el portento tecnológico que son los diferentes Galaxy S6, o el Note 5. Si le hacemos caso a antiguos trabajadores y directivos, Samsung está más enfocada a vender mucho a corto plazo, que a mejorar su software.
Samsung está demasiado enfocada a crear el mejor hardware, y ahí podemos ponerle pocas pegasEs lo que nos cuentan desde Reuters, criticando que lo ideal sería mantener clientes mucho tiempo, algo que sería posible con una plataforma software más pulida y especial. Si echamos un vistazo a la propuesta, pocas cosas ofrecen sobre el resto de fabricantes Android. 
Samsung está demasiado enfocada a crear el mejor hardware, y ahí podemos ponerle pocas pegas
Lo más sonado puede ser Samsung Pay, y no parece ser un factor de compra. En los últimos años hemos visto morir servicios como ChatOn o Milk, ambos bien promocionados y creados, pero sin éxito comercial, a pesar de venir instalados dentro de cada terminal.
A comienzos de mes conocimos que había cambio en la cúpula de Samsung Mobile, J.K. Shin dejaba su sitio a Dongjin Koh - responsable de Samsung Pay y Tizen -, con la intención de que haya un cambio de dirección en la compañía, que tiene por delante bastante trabajo en cambiar una cultura basada en la creación de hardware.
J.K. Shin ha estado trabajando en Samsung a todos los niveles desde 1984
En Samsung se produce un cambio cultural lento, deben dar prioridad a servicios y softwareMientras hablamos de todo esto, Samsung sigue vendiendo a buen ritmo, y según consultoras como TrendForce, se estima que van a  vender cien millones de teléfonos más que su más directo rival, Apple. 
En Samsung se produce un cambio cultural lento, deben dar prioridad a servicios y software
El problema está en la cuota de mercado, que puede bajar un 20% este 2015, principalmente por el acoso de las marcas chinas.
Para Samsung su división móvil representa el 39% de los beneficios, según lo recopilado en los nueve primeros meses del año. Suena importante, pero más lo era en 2013, cuando el porcentaje estaba en un 68%. En 2015 se van a mover en cifras parecidas a 2010.
También te puede gustar
Recomendado en Magnet
+ en Xataka
Tecnología
Estilo de vida
Motor
Ocio
Economía
Latinoamérica
Participamos en
