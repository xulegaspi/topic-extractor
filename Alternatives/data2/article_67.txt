http://www.xataka.com/moviles/fujitsu-le-da-la-independencia-a-sus-divisiones-de-telefonos-y-ordenadores
Gadgets y tecnología: últimas tecnologías en electrónica de consumo - XATAKA






Buscar »

Google+
@kotecinho
Editor senior en Xataka
Así es cómo se consigue convertir un tablet en un equipo de sonido 5.1
Los tablets tienen un problema serio con el sonido. ¿Por qué suena tan enlatado? Así es la solución de Huawei.

Espero no aburriros demasiado con el asunto, pero me parece relevante conocer que Fujitsu quiere separar dos negocios vitales en su dilatada historia en el mercado tecnológico, para formar empresas independientes. 
La compañía japonesa lo quiere hacer con los teléfonos, una división menos conocida por estos lares, pero muy importante en su mercado natal. También lo quiere hacer con los ordenadores, mercado donde sí ha operado en Europa desde hace muchos años.
Podemos entender que esas nuevas compañías serán subsidiarias, en realidad seguirán teniendo el mismo dueño, pero la forma de operar será más independiente, con mayor capacidad de riesgo y menos consulta para los movimientos importantes. Esto parece una tontería, pero como está el mercado de competido ahora mismo, o apuestas por cambios, o desapareces.
La que se va a dedicar a los ordenadores se va a llamar Fujitsu Client Computing Limited, la del negocio de los teléfonos, Fujitsu Connected Technologies Limited. A final de cuentas, cuando veamos productos en la calle, los llamaremos Fujitsu ‘tal’ o Fujitsu ‘cual’, pero es interesante conocer los cambios internos.
¿Para qué separar? Fujitsu considera que es muy difícil conseguir diferenciarse en estos mercados, la competición de marcas que operan de forma global se ha intensificado demasiado, y hacer dinero vendiendo ordenadores o teléfonos es una tarea ardua. Los teléfonos chinos son demasiado baratos, y los PCs se han estancadao en su crecimiento.
La otra razón importante por la que una gran compañía quiere hacer funcionar a sus divisiones de forma independiente puede ser una venta. Si quieres vender o realizar asociaciones con otras empresas, para sobrevivir, este camino es una opción.
A comienzos de año se había rumoreado que Fujitsu, Toshiba y la nueva VAIO querían combinar operaciones para ser más fuertes en el mundo de los ordenadores. Lo mismo este movimiento que os explicamos hoy tiene algo que ver con esto. Sumar sus fuerzas sería conseguir el 30% de la cuota de mercado en Japón.
Las empresas japonesas tienen como prioridad ser muy fuertes en su país natal, pero también son capaces de unir fuerzas si es necesario competir con lo que hay fuera. El mejor ejemplo lo tenemos con Japan Display, una empresa que ‘hace pantallas’ y es propiedad de las tres grandes (Sony, Toshiba, Hitachi). Su intención es ser competitiva contra el imperio coreano, y más o menos les está saliendo bien.
Vía | Engadget
Más información | Fujitsu
También te puede gustar
Recomendado en Magnet
+ en Xataka
Tecnología
Estilo de vida
Motor
Ocio
Economía
Latinoamérica
Participamos en
