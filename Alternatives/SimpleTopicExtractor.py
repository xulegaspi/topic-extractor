__author__ = 'Xurxo'
import nltk
from textblob.classifiers import NaiveBayesClassifier
import json

train = [
     ('I love this sandwich.', 'pos'),
     ('this is an amazing place!', 'pos'),
     ('I feel very good about these beers.', 'pos'),
     ('this is my best work.', 'pos'),
     ("what an awesome view", 'pos'),
     ('I do not like this restaurant', 'neg'),
     ('I am tired of this stuff.', 'neg'),
     ("I can't deal with this", 'neg'),
     ('he is my sworn enemy!', 'neg'),
     ('my boss is horrible.', 'neg')
]

# a1 = open("dataset/article_1.txt", 'r').read().decode('utf-8')
# a2 = open("dataset/article_2.txt", 'r').read().decode('utf-8')
# a3 = open("dataset/article_3.txt", 'r').read().decode('utf-8')
# a4 = open("dataset/article_4.txt", 'r').read().decode('utf-8')
# a5 = open("dataset/article_5.txt", 'r').read().decode('utf-8')
# a6 = open("dataset/article_6.txt", 'r').read().decode('utf-8')
# a7 = open("dataset/article_7.txt", 'r').read().decode('utf-8')
# a8 = open("dataset/article_8.txt", 'r').read().decode('utf-8')
# a9 = open("dataset/article_9.txt", 'r').read().decode('utf-8')
# a10 = open("dataset/article_10.txt", 'r').read().decode('utf-8')
# a11 = open("dataset/article_11.txt", 'r').read().decode('utf-8')
# a12 = open("dataset/article_12.txt", 'r').read().decode('utf-8')
#
a1 = open("dataset/article_1.txt", 'r').read()
a2 = open("dataset/article_2.txt", 'r').read()
a3 = open("dataset/article_3.txt", 'r').read()
a4 = open("dataset/article_4.txt", 'r').read()
a5 = open("dataset/article_5.txt", 'r').read()
a6 = open("dataset/article_6.txt", 'r').read()
a7 = open("dataset/article_7.txt", 'r').read()
a8 = open("dataset/article_8.txt", 'r').read()
a9 = open("dataset/article_9.txt", 'r').read()
a10 = open("dataset/article_10.txt", 'r').read()
a11 = open("dataset/article_11.txt", 'r').read()
a12 = open("dataset/article_12.txt", 'r').read()
a13 = open("dataset/article_13.txt", 'r').read()
a14 = open("dataset/article_14.txt", 'r').read()
a15 = open("dataset/article_15.txt", 'r').read()
a16 = open("dataset/article_16.txt", 'r').read()
a17 = open("dataset/article_17.txt", 'r').read()
a18 = open("dataset/article_18.txt", 'r').read()
a19 = open("dataset/article_19.txt", 'r').read()
a20 = open("dataset/article_20.txt", 'r').read()
a21 = open("dataset/article_21.txt", 'r').read()
a22 = open("dataset/article_22.txt", 'r').read()
a23 = open("dataset/article_23.txt", 'r').read()
a24 = open("dataset/article_24.txt", 'r').read()
a25 = open("dataset/article_25.txt", 'r').read()


train2 = [
    (a1, 'Space'),
    (a2, 'Space'),
    (a3, 'Space'),
    (a4, 'Space'),
    (a5, 'Space'),
    (a6, 'Politic'),
    (a7, 'Computer'),
    (a8, 'Mobile'),
    (a9, 'Tablet'),
    (a10, 'Computer'),
    (a11, 'Space'),
    (a12, 'Space')
]

train3 = [
    {"text": a1, "label": "Space"},
    {"text": a2, "label": "Space"},
    {"text": a3, "label": "Space"},
    {"text": a4, "label": "Space"},
    {"text": a5, "label": "Space"},
    {"text": a6, "label": "Politic"},
    {"text": a7, "label": "Computer"},
    {"text": a8, "label": "Mobile"},
    {"text": a9, "label": "Tablet"},
    {"text": a10, "label": "Computer"},
    {"text": a11, "label": "Space"},
    {"text": a12, "label": "Space"},
    {"text": a13, "label": "Space"},
    {"text": a14, "label": "Space"},
    {"text": a15, "label": "Space"},
    {"text": a16, "label": "Artificial Intelligence"},
    {"text": a17, "label": "Car"},
    {"text": a18, "label": "Space"},
    {"text": a19, "label": "Space"},
    {"text": a20, "label": "Space"},
    {"text": a21, "label": "Space"},
    {"text": a22, "label": "Genetic"},
    {"text": a23, "label": "Climate"},
    {"text": a24, "label": "Climate"},
    {"text": a25, "label": "Privacy"}
]

# out_aux = open('dataset/ss.json', 'w')
# out_aux.write(json.dumps(train3))
# out_aux.close()

# cl = NaiveBayesClassifier(train3, format="json")
with open('train.json', 'r') as fp:
    cl = NaiveBayesClassifier(fp, format="json")

counter = 1
while counter < 50:
    with open('data/article_' + str(counter) + '.txt', 'r') as fp:
        text = fp.read()
        resul = cl.classify(text)
        # print text
        print str(counter) + " -> " + resul + " with prob: " + str(round(cl.prob_classify(text).prob(resul), 5))
    fp.close()
    counter += 1