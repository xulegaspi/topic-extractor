http://www.engadget.com/2015/06/28/spacex-falcon-9-rocket-breaks-up/
Today is not a good day for private spaceflight. SpaceX and NASA have confirmed that a Falcon 9 rocket broke up shortly after launching on a resupply mission for the International Space Station. It's not clear what caused the failure at this stage, but the vehicle started smoking right before it fell apart. The destruction won't create immediate problems for the Space Station. However, it comes at a very bad time for SpaceX. The company has lost yet another chance to land its reusable rocket on a sea barge, and it only just got clearance to launch valuable missions for both NASA and the US Air Force -- Elon Musk and crew may have to work overtime assuring officials that this kind of disaster won't happen again.
Update: Musk reports that the explosion might have been due to an "overpressure event" in the rocket's upper-stage liquid oxygen tank. At a press conference, neither NASA nor SpaceX had enough information to say what went wrong besides ruling out a problem with the first (lower) stage. NASA notes that the ISS crew still has a comfortable four-month supply buffer, but is understandably concerned that there have been three supply mission failures in the space of several months.  Microsoft is no doubt worried, too, since the HoloLens headsets for its astronaut assistance project were on the SpaceX flight.
[Image credit: SpaceX, Flickr]




Your dating matches are based partly on an largely unknown algorithm.
The two daily fantasy sites were awarded a permanent stay by a panel in New York. 
A night mode, Apple TV folders and multiple iPad users (in school) are coming.
Sadly, you won't be able to activate this lesser model with your voice.
A cryptic Facebook post got our hopes up.
