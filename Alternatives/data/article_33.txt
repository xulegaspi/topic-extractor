http://www.engadget.com/2015/11/02/pentagon-launches-cybersecurity-exchange-to-combat-hackers/

The Department of Defense is taking a number of steps to up its cybersecurity game, Terry Halvorsen, the Pentagon's Chief Information Officer recently told a reporters breakfast hosted by The Christian Science Monitor. "There's not a time when I'm not being attacked somewhere in the world," Halvorsen said. "We're looking to industry to help us solve some specific areas." To that end, the DoD has begun assigning its civilian personnel to 6-month tours of private cybersecurity companies, such as Cisco, as well as inviting employees from those firms to help train its personnel to defend the DoD's networks against hacks.
The internet is "important part of our business, an important part of our culture, but you have to go there with the right rules and right understandings," Halvorson continued. As such, he is pushing for a "culture of cyber discipline" within the Pentagon. That includes automating many of their networks defenses so that they can automatically recognize, quarantine and halt attacks without human intervention.
Your dating matches are based partly on an largely unknown algorithm.
The two daily fantasy sites were awarded a permanent stay by a panel in New York. 
A night mode, Apple TV folders and multiple iPad users (in school) are coming.
Sadly, you won't be able to activate this lesser model with your voice.
A cryptic Facebook post got our hopes up.
