http://www.engadget.com/2015/10/08/elon-musk-apple-car-diss/

During an interview with German business newspaper Handelsblatt on Thursday, Elon Musk unleashed some very CE-Oh No He Didn't!-worthy words about Apple's car efforts. "They have hired people we've fired," he responded when asked if he was worried about a new competitor that's been snapping up former Tesla engineers. He even revealed that they (he and his friends from the auto company, presumably) jokingly call Apple the Tesla Graveyard. "If you don't make it at Tesla, you go work at Apple," he added and made it a point to clarify that he wasn't kidding with that one.
Update (10/9): Today Musk to Twitter with a few (almost?) apologies and compliments for Apple, saying it has great people and that by version three, the Watch will actually be worthwhile.
When asked if he takes Apple's plans seriously, he took a jab at one of Cupertino's newest devices, as well as the fruit company's typical manufacturing process, which involves Taiwanese contractor Foxconn:
Did you ever take a look at the Apple Watch? (laughs) No, seriously: It's good that Apple is moving and investing in this direction. But cars are very complex compared to phones or smartwatches. You can't just go to a supplier like Foxconn and say: Build me a car. But for Apple, the car is the next logical thing to finally offer a significant innovation. A new pencil or a bigger iPad alone were not relevant enough.
Musk didn't go to Germany just to diss Apple, though: he went there to talk with government representatives about the future of mobility and electric vehicles in the country. He admitted that financial and other incentives for EVs -- such as allowing them to use the bus lanes like Oslo, Norway does -- would be extremely helpful in Tesla's expansion plans.
He also didn't hold back when asked what the German automakers are doing wrong, explaining that they're trying to cling to the past. Musk boldly proclaimed that we've reached the limit of what gasoline can do, and it's time to build the next generation of automobiles. "You see what's happened with the current diesel scandal at Volkswagen," he said, using the automaker's emission-cheating car scandal as an example. "In order to make progress, they apparently had to cheat. I think if you intentionally mislead governments around the world with software that is designed to only be effective at the test stand, this is a very conscious action."
Yo, I don't hate Apple. It's a great company with a lot of talented people. I love their products and I'm glad they're doing an EV.
Regarding the watch, Jony & his team created a beautiful design, but the functionality isn't compelling yet. By version 3, it will be.
[Image credit: pestoverde/Flickr]
Starting with hip-hop, the streaming service offers explanations of lyrics with help from Genius.
The Planetary Defense Coordination Office is real and not an organization from a sci-fi flick. 
UK police reports involving Grindr or Tinder rose 700 percent in two years.
Watch periscopes without actually downloading Periscope.
Those who don't upgrade will be more susceptible to malicious attacks.
