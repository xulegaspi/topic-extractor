http://www.engadget.com/2015/12/20/spacex-ground-landing/
SpaceX has been understandably quiet since its last rocket exploded right after launch, but it's hoping to make up for that failure in style. It's planning to not only launch a Falcon 9 rocket on December 21st at 8:29PM ET, but attempt its first-ever ground landing with that rocket -- no doubt in hopes of countering Blue Origin's landing from a few weeks ago. Even if the touchdown fails, it'll still be an important launch as ORBCOMM gets 11 communications satellites into orbit. Tune in to the live stream below (coverage should start at 8:05PM) and you'll see whether or not SpaceX has better success on terra firma than it did at sea.
Update (12/21): You can watch the live stream of the launch attempt right here.
[Image credit: SpaceX Photos, Flickr]
Just reviewed mission params w SpaceX team. Monte Carlo runs show tmrw night has a 10% higher chance of a good landing. Punting 24 hrs.— Elon Musk (@elonmusk) December 20, 2015 
Just reviewed mission params w SpaceX team. Monte Carlo runs show tmrw night has a 10% higher chance of a good landing. Punting 24 hrs.

Starting with hip-hop, the streaming service offers explanations of lyrics with help from Genius.
The Planetary Defense Coordination Office is real and not an organization from a sci-fi flick. 
UK police reports involving Grindr or Tinder rose 700 percent in two years.
Watch periscopes without actually downloading Periscope.
Those who don't upgrade will be more susceptible to malicious attacks.
