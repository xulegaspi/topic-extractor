http://www.engadget.com/2015/01/28/spacex-falcon-heavy-animation/
SpaceX's Falcon Heavy has yet to grace a launch pad, but that isn't stopping the company from extolling the reusable rocket's virtues.  Elon Musk and crew have posted an animation (below) demonstrating how a typical mission with the heavy-duty reusable rocket should go.  As you might imagine, everything goes smoothly in this conceptual clip -- the machine blasts off from Kennedy Space Center, detaches its Falcon 9 boosters (which dutifully return to the ground) and puts its payload into orbit.  Success!
The real question is whether or not SpaceX can live up to its vision.  The company has done a lot to advance private spaceflight, but some of its experiments with reusable rockets haven't gone according to plan.  Hopefully, the company has put as much thought into Falcon Heavy's inaugural flight (due this year) as it has the marketing blitz.

Starting with hip-hop, the streaming service offers explanations of lyrics with help from Genius.
The Planetary Defense Coordination Office is real and not an organization from a sci-fi flick. 
UK police reports involving Grindr or Tinder rose 700 percent in two years.
Watch periscopes without actually downloading Periscope.
Those who don't upgrade will be more susceptible to malicious attacks.
