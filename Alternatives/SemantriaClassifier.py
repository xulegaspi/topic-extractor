__author__ = 'Xurxo'
import semantria
import time
import uuid

key = "a663b90e-60c7-4ea7-896a-6a4f02d5693a"
secret = "3aa96436-ba1d-4b4a-beaa-7ff2d0d1076c"

filename = "data/article_23.txt"

session = semantria.Session(key, secret)
# print session

aux = open(filename, 'r').read()
initialTexts = [aux]

for text in initialTexts:
    doc = {"id": str(uuid.uuid4()).replace("-", ""), "text": text}
    status = session.queueDocument(doc)
    if status == 202:
        print("\"", doc["id"], "\" document queued successfully.", "\r\n")

length = len(initialTexts)
results = []

while len(results) < length:
    print("Retrieving your processed results...", "\r\n")
    time.sleep(2)
    # get processed documents
    status = session.getProcessedDocuments()
    results.extend(status)

for data in results:
    # print document sentiment score
    print("Document ", data["id"], " Sentiment score: ", data["sentiment_score"], "\r\n")

    # print document themes
    if "themes" in data:
        print("Document themes:", "\r\n")
        for theme in data["themes"]:
            print("     ", theme["title"], " (sentiment: ", theme["sentiment_score"], ")", "\r\n")

    # print document entities
    if "entities" in data:
        print("Entities:", "\r\n")
        for entity in data["entities"]:
            print("\t", entity["title"], " : ", entity["entity_type"], " (sentiment: ", entity["sentiment_score"], ")",
                  "\r\n")

    # print document topics
    if "topics" in data:
        print("Topics:", "\r\n")
        for topic in data["topics"]:
            print("\t", topic["title"], " : ", topic["type"], " (sentiment: ", topic["sentiment_score"], ")", "\r\n")
