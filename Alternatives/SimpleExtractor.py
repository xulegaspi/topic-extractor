__author__ = 'Xurxo'
import urllib
from bs4 import BeautifulSoup
import urlparse
import mechanize

max_links = 50
filename = "dataset_engadget.txt"
folder = "dataset"

br = mechanize.Browser()

urls = []

file = open(filename, 'r')
for line in file:
    urls.append(line[:len(line) - 1])

print len(urls)

counter = 0
for url in urls:
    if counter == max_links:
        break
    try:
        print url
        htmltext = urllib.urlopen(url).read()
        counter += 1
    except:
        print "ERROR"

    soup = BeautifulSoup(htmltext, "html.parser")

    out = open(folder + "/article_" + str(counter) + ".txt", 'w')
    out.write(url + "\n")
    for p in soup.findAll('p'):
        print p.text
        try:
            out.write(p.text.encode('utf-8') + '\n')
        except:
            out.write("ERROR\n")

    out.close()


