__author__ = 'Xurxo'
import json
import csv
from datetime import datetime

input_filename = "results/data_mined.json"
output_filename = "results/data1.csv"

categories = []
dates = []
counts = []
rows = []

with open(input_filename, 'r') as in_file:
    data = json.load(in_file)
    for entry in data:
        for category in entry['categories']:
            date = entry['pubDate']
            oldtime = date['date']
            try:
                date_object = datetime.strptime(oldtime, '%Y%m%dT%H%M%S')
                form_date = date_object.strftime('%d-%m-%Y')
                month = date_object.strftime("%m")
            except:
                form_date = ""

            if category['label'] not in categories:
                categories.append(category['label'])
                dates.append(form_date)
                counts.append(1)
            else:
                index = categories.index(category['label'])
                counts[index] += 1
                # print "REPEAT"
        # print json.dumps(entry['categories'])
    # print json.dumps(data)
            aux = [form_date, category['label']]
            rows.append(aux)

    ######################################
    # Category -> count
    ######################################

    # length = len(categories)
    # # print length
    # counter = 0
    # while counter < length:
    #     # print dates[counter] + " -> " + categories[counter] + " -> " + str(counts[counter])
    #     print categories[counter] + " -> " + str(counts[counter])
    #     counter += 1

    ######################################
    # [month, category]
    ######################################

    # with open(output_filename, 'w') as csvfile:
    #     fieldnames = ['Month', 'Category']
    #     writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    #     writer.writeheader()
    #
    #     for row in rows:
    #         # print row
    #         writer.writerow({'Month': row[0], 'Category': row[1]})

    with open("results/data2.txt", 'w') as txtfile:
        txtfile.write("Month\tCategory\n")
        for row in rows:
            # print row
            # writer.writerow({'Month': row[0], 'Category': row[1]})
            txtfile.write(row[0] + "\t" + row[1] + "\n")
        txtfile.close()